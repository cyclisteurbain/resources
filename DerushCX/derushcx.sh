#!/bin/bash
#########################################################################################
# DerushCX Script                                                                       #
# >> Automates extraction of rushes marked though the GoPro HIlight feature in a set    #
#    of video files                                                                     #
#  @CyclisteUrbain with precious help from @olivelo and @ticoli                         #
#                                                                                       #
# v1.0 - Initial version                                                                #
# v1.1 - Ignore Hilights too close, support for switches, fixed "extraction start" test #
# v1.2 - Added support for suffixes on output files, changed the prefix switch          #
# v1.3 - Corrected time naming from GoPro8+ secondary files                             #
# v1.4 - Anonymisation for public release                                               #
#                                                                                       #
#                                                                                       #
#                                                             <!         @              #
#                                                              !___    _`\<,_           #
# Make streets safer for cyclists, show the world.             !(*)!--(*)/(*)           #
#########################################################################################



#SETTINGS
#########################################################################################

#>> Source directory (with a trailing "/")
SET_sourcedir="~/Documents/GoPro/Source/"

#>> Output directory (with a trailing "/")
SET_outputdir="~/Documents/GoPro/Output/"

#>> Seconds before Hilight that should be extracted
SET_extractdurationbefore=55

#>> Seconds after Hilight that should be extracted
SET_extractdurationafter=5

#>> Minimum time between two Hilight (will ignore the second Hilight)
SET_mindelta=5

#>> Prefix of generated files
SET_fileprefix=""

#>> Suffix of generated files
SET_filesuffix=""

#>> Loging directory (with a trailing "/")
SET_logfiledir="~/Documents/GoPro/Log"

#>> If set to true, will delete the original files after a successful extraction
#Not yet implemented
SET_deleteafter=false


#Colors
RED='\033[1;31m'
GRN='\033[1;32m'
BLU='\033[1;34m'
YLW='\033[1;33m'
BLD='\033[1;1m'
NC='\033[0m'

#PARSES SWITCHES
#########################################################################################
SOURCEDIR=$SET_sourcedir
OUTPUTDIR=$SET_outputdir
BEFORE=$SET_extractdurationbefore
AFTER=$SET_extractdurationafter
MINDELTA=$SET_mindelta
PREFIX=$SET_fileprefix
SUFFIX=$SET_filesuffix
DELETE=$SET_deleteafter


POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -s|--sourcedir)
        SOURCEDIR="$2"
        shift # past argument
        shift # past value
        ;;
        -o|--outputdir)
        OUTPUTDIR="$2"
        shift # past argument
        shift # past value
        ;;
        -b|--before)
        BEFORE="$2"
        shift # past argument
        shift # past value
        ;;
        -a|--after)
        AFTER="$2"
        shift # past argument
        shift # past value
        ;;
        -m|--mindelta)
        MINDELTA="$2"
        shift # past argument
        shift # past value
        ;;
        -px|--prefix)
        PREFIX="$2"
        shift # past argument
        shift # past value
        ;;
        -sx|--suffix)
        SUFFIX="$2"
        shift # past argument
        shift # past value
        ;;
        -d|--delete)
        DELETE=YES
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters




#STARTS LOGGING
#########################################################################################

#Formats log file name
logtime=$(date +"%Y%m%d-%H%M")
logfilename="derushcx_"$logtime".log"
logfilepath=$SET_logfiledir$logfilename

#Initiates information
MSG1="DerushCX script started at $(date +"%H:%M:%S on %d/%m/%Y")"
echo -e "${BLD}$MSG1${NC}"
echo $MSG1 >> $logfilepath
MSG1="Version 1.3 - 15/03/2022"
echo -e "${BLD}$MSG1${NC}"
echo $MSG1 >> $logfilepath
MSG1="Logging execution information to "$logfilepath
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="Input directory : "$SOURCEDIR
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="Output directory : "$OUTPUTDIR
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="File prefix : "$PREFIX
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="Seconds before hilight : "$BEFORE
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="Seconds after hilight : "$AFTER
echo $MSG1
echo $MSG1 >> $logfilepath
MSG1="Minimum time between hilights : "$MINDELTA
echo $MSG1
echo $MSG1 >> $logfilepath

#PRE PROCESSING CHECKS
#########################################################################################




#Checks that directories exist
if [ ! -d "$SOURCEDIR" ]; then
    MSG1="ERROR : Source directory "$SOURCEDIR" doesn't exist, terminating script"
    echo -e "${RED}$MSG1${NC}"
    echo $MSG1 >> $logfilepath
    exit 1
fi

if [ ! -d "$OUTPUTDIR" ]; then
    MSG1="ERROR : Output directory "$OUTPUTDIR" doesn't exist, terminating script"
    echo -e "${RED}$MSG1${NC}"
    echo $MSG1 >> $logfilepath
    exit 1
fi

#CORE PROCESSING SECTION
#########################################################################################
MSG1="Starting looping through files in source directory"
echo $MSG1
echo $MSG1 >> $logfilepath

files=$SOURCEDIR"G*.MP4"

file_cnt=0
filewithhilights_cnt=0
totalhilights_cnt=0
totalhilightssuccess_cnt=0
totalhilightserror_cnt=0
totalhilightignored_cnt=0
#Loops through files in source dir
for file in $files
    do
    ((file_cnt++))
    MSG1="\nProcessing file "$file
    echo -e "${GRN}$MSG1${NC}"
    echo $MSG1 >> $logfilepath

    #Gathers information about the file
    filemeta=$(ffprobe -v error -show_format -show_chapters -print_format json $file)
            create_date=$(echo $filemeta | jq -r .format.tags.creation_time | cut -c 1-19 ) #Cuts off timezone info as its bogus
            create_date=$(echo $create_date$(gdate  +"%z")) #And add the locale one
            duration=$(echo $filemeta | jq -r .format.duration | awk '{print int($1)}')
            filesize=$(echo $filemeta | jq -r .format.size)
            firmware=$(echo $filemeta | jq -r .format.tags.firmware)
            bitrate=$(echo $filemeta | jq -r .format.bit_rate)

    filename=$(basename "$file")
    extension="${filename##*.}"

    #Get the camera/fw informations
    camversion=$(echo $firmware | cut -c 3-3)
    fwversion=$(echo $firmware | cut -c 6-15)
    MSG1=">> File generated by GoPro "$camversion" with firmware v"$fwversion
    echo -e "${NC}$MSG1${NC}"
    echo $MSG1 >> $logfilepath

    #If this is a GoPro8, gets the file number and the duration of a complete file to correct the create_date variable
    if [[ $camversion -eq 8 ]]; then

        #File number
        filenumber=$(echo $filename | cut -c 4-4)
        ((filesbefore=filenumber-1))

        if [[ $filesbefore -gt 0 ]]; then
            MSG1="   It's a secondary file from a GoPro 8, so let's correct the <create_date> information"
            echo -e "${NC}$MSG1${NC}"
            echo $MSG1 >> $logfilepath

            #First file duration
            firstfile=$SOURCEDIR$(echo $filename | cut -c 1-3)"1"$(echo $filename | cut -c 5-15)
            MSG1="   Getting duration information from initial file "$firstfile
            echo -e "${NC}$MSG1${NC}"
            echo $MSG1 >> $logfilepath

            firstfilemeta=$(ffprobe -v error -show_format -show_chapters -print_format json $firstfile)
            firstduration=$(echo $firstfilemeta | jq -r .format.duration | awk '{print int($1)}')

            ((startoffset=filesbefore*firstduration))
            MSG1="   Offset is "$startoffset" seconds"
            echo -e "${NC}$MSG1${NC}"
            echo $MSG1 >> $logfilepath

            create_date=$(gdate +"%Y-%m-%dT%H:%M:%S%z" -d "$create_date + $startoffset seconds")
            MSG1="   Actual create_date is "$(gdate '+%d/%m/%Y %H:%M:%S%z' -d "$create_date")
            echo -e "${NC}$MSG1${NC}"
            echo $MSG1 >> $logfilepath
        fi

    fi

    #Shows general information
        #Creation Date
        create_date_show=$(date -j -f "%Y-%m-%dT%H:%M:%S%z" $create_date +"%d/%m/%Y %H:%M:%S")
        MSG1=">> Shot on : "$create_date_show
        echo -e "${NC}$MSG1${NC}"
        echo $MSG1 >> $logfilepath

        #Duration
        duration_show=$(printf '%dh:%dm:%ds\n' $(($duration/3600)) $(($duration%3600/60)) $(($duration%60)))
        MSG1=">> Duration : "$duration_show
        echo -e "${NC}$MSG1${NC}"
        echo $MSG1 >> $logfilepath

        #Size
        filesize_show=$(echo "${filesize}" | awk '{ split( "B KB MB GB TB PB" , v ); s=1; while( $1>1024 ){ $1/=1024; s++ } printf "%.2f %s", $1, v[s] }')
        MSG1=">> File size : "$filesize_show
        echo -e "${NC}$MSG1${NC}"
        echo $MSG1 >> $logfilepath

        #Chapters/Hilights
        hilight_count=0
        for hi in $(echo $filemeta | jq -r .chapters[].start_time| awk '{print int($1)}')
           do
           ((hilight_count++))
           hi_time=$(printf '%dh:%dm:%ds\n' $(($hi/3600)) $(($hi%3600/60)) $(($hi%60)))
           MSG1=">> Hilight : "$hi_time
           echo -e "${NC}$MSG1${NC}"
           echo $MSG1 >> $logfilepath
        done
        if (($hilight_count)); then
           ((filewithhilights_cnt++))
           MSG1=">> "$hilight_count" hilight(s) detected"
           echo -e "${NC}$MSG1${NC}"
           echo $MSG1 >> $logfilepath
        else
           MSG1="** No hilight was detected in file"
           echo -e "${YLW}$MSG1${NC}"
           echo $MSG1 >> $logfilepath
        fi

       #Extracts around the Hilight if some were detected
       if (($hilight_count)); then
            MSG1="Extraction of the "$hilight_count" hilight(s)"
            echo -e "${NC}$MSG1${NC}"
            echo $MSG1 >> $logfilepath

            hilight_count=0
            hilightlast=0 #Last processed highlight position (for ignoring)
            for hipos in $(echo $filemeta | jq -r .chapters[].start_time| awk '{print int($1)}')
                do
                ((totalhilights_cnt++))
                ((hilight_count++))

                delta=$((hipos-hilightlast))
                if [[ $delta -lt $MINDELTA ]]; then
                    MSG1="** Extraction of hilight #"$hilight_count" at "$hipos"s ignored as it's "$delta"s from previous at "$hilightlast"s"
                    echo -e "${YLW}$MSG1${NC}"
                    echo $MSG1 >> $logfilepath
                    ((totalhilightignored_cnt++))
                else

                    MSG1=">>Extraction of hilight #"$hilight_count
                    echo -e "${NC}$MSG1${NC}"
                    echo $MSG1 >> $logfilepath

                    #extraction start
                    extract_start=$((hipos-BEFORE))
                    if [[ $extract_start -lt 0 ]]; then
                        extract_start=0
                    fi

                    #extraction duration
                    duration=$(($BEFORE+$AFTER))

                    #file name
                    filenamedate=$(gdate '+%Y%m%d_%H%M%S' --date="$create_date + $extract_start seconds")
                    outputfile=$OUTPUTDIR$PREFIX$filenamedate$SUFFIX"."$extension

                    MSG1="Output to "$outputfile
                    echo -e "${NC}$MSG1${NC}"
                    echo $MSG1 >> $logfilepath

                    ffmpeg -loglevel panic -ss $extract_start -t $duration -i $file -c copy $outputfile

                    ##Simulation
                    #echo "Simulation"
                    #echo "ffmpeg -loglevel panic -ss "$extract_start" -t "$duration" -i "$file" -c copy "$outputfile

                    if (($?)); then
                        ((totalhilightserror_cnt++))
                        MSG1="!! ERROR : extraction of hilight #"$hilight_count" failed from "$file
                        echo -e "${RED}$MSG1${NC}"
                        echo $MSG1 >> $logfilepath
                    else
                        ((totalhilightssuccess_cnt++))
                        MSG1=">> Extraction of hilight #"$hilight_count" succeded"
                        echo -e "${GRN}$MSG1${NC}"
                        echo $MSG1 >> $logfilepath
                    fi

                fi
                hilightlast=$hipos
            done
        fi
done
#<<Loop through files

#Closes logfiles
MSG1="DerushCX script stopped at $(date +"%H:%M:%S on %d/%m/%Y")"
echo -e "${BLD}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Total files scanned : "$file_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Files with hilights : "$filewithhilights_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Total number of hilights : "$totalhilights_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Successful extractions : "$totalhilightssuccess_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Ignored hilights : "$totalhilightignored_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath

MSG1=">> Failed extractions : "$totalhilightserror_cnt
echo -e "${NC}$MSG1${NC}"
echo $MSG1 >> $logfilepath
