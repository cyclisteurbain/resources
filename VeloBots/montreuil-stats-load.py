#!/usr/bin/python3

##############################################
# IMPORTS
import argparse
import json
from datetime import datetime, timedelta
from os import path
from sqlalchemy import create_engine, text
from sqlalchemy import text
import requests
import pytz
import hashlib

##############################################
# SETTINGS AND INIT

#Importing config file
import xxxx_stats_config as config
from xxxx_stats_functions import *

set_logpath = config.LOGPATH

locale_tz = pytz.timezone("Europe/Paris")
logfile = None
two_ways_record = True 

#latest data for dataset update
latest_timestamp = datetime(1970,1,1,0,0,0,0,pytz.UTC)

#Default start date
default_startdate = datetime.strftime(datetime.now() - timedelta(days=1),"%d/%m/%Y")

##############################################
# FUNCTIONS

def is_naive(d):
    if(d.tzinfo is None or d.tzinfo.utcoffset(d) is None):
        logmessage("Date "+datetime.strftime(d,"%d/%m/%Y %H:%M:%S%z")+" is NAIVE",logfile,1)
    else:
        logmessage("Date "+datetime.strftime(d,"%d/%m/%Y %H:%M:%S%z")+" is AWARE",logfile,1)
    

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Let's go!")

logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* montreuil-stats.py                                                                 *",logfile,2)
logmessage("*    Queries the data.montreuil.fr API to retrieve bike counters data                *",logfile,2)
logmessage("*    and adds it to a MySQL database                                                 *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*   Version 1.0 - 16/10/2022 - Initial version                                       *",logfile,2)
logmessage("*   Version 1.1 - 17/10/2022 - Default date is day before                            *",logfile,2)
logmessage("*   Version 1.2 - 17/10/2022 - Checksum support                                      *",logfile,2)
logmessage("*   Version 1.3 - 27/10/2022 - Configuration in a separate file                      *",logfile,2)
logmessage("*   Version 1.4 - 18/11/2022 - Handeling of KeyError in records                      *",logfile,2)
logmessage("*   Version 1.5 - 27/12/2022 - Switch to SQL Alchemy                                 *",logfile,2)
logmessage("*   Version 1.5b- 08/04/2023 - Switch to common config/functions, removed unused mod *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1)

global_chronostart = datetime.now()
##############################################
# ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--datasetid', '-d', help="ID of the dataset as referenced in the database", type= str, default= "")
parser.add_argument('--startdate', '-s', help="Start date of the data that will be fetched as dd/mm/aaaa. Set to the day before if not provided", type= str, default=default_startdate)
parser.add_argument('--enddate',   '-e', help="End date of the data that will be fetched as dd/mm/aaaa. Set to the start date if not provided", type= str, default= "")
args = parser.parse_args()

#Dataset is provided
if args.datasetid == "":
    logmessage("Dataset ID was not provided altough it's mandatory",logfile,5)
    print(parser.format_help())
    quit()  
target_dataset_id = args.datasetid
logmessage("Target Dataset ID : "+target_dataset_id,logfile,1)

#StartDate is an actual date
try:
    start_date = datetime.strptime(args.startdate,"%d/%m/%Y")
except Exception as oups:
    logmessage("Provided start date argument '"+args.startdate+"' is not a valid date",logfile,5)
    print(parser.format_help())
    quit()  

#StartDate is in the past
start_date_age = datetime.now() - start_date 
if(start_date_age.total_seconds() < 0):
    logmessage("Provided start date argument '"+args.startdate+"' is is in the future",logfile,5)
    quit()
logmessage("Start date : "+datetime.strftime(start_date,"%d-%m-%Y"),logfile,1)

#EndDate defaulted to Start Date if not provided, and if provided, checked if it's a date after Start Date
if(args.enddate == ""):
    end_date = start_date
else:
    try:
        end_date = datetime.strptime(args.enddate,"%d/%m/%Y")
    except Exception as oups:
        logmessage("Provided end date argument '"+args.enddate+"' is not a valid date",logfile,5)
        print(parser.format_help())
        quit()
    end_date_diff = end_date - start_date
    if(end_date_diff.total_seconds() < 0):
        logmessage("Provided end date argument '"+args.startdate+"' is after start date",logfile,5)
        quit()

logmessage("End date : "+datetime.strftime(end_date,"%d-%m-%Y"),logfile,1)


##############################################
# RETRIEVES DATASET INFO FROM DB
logmessage("Retreiving metadata from database for dataset "+target_dataset_id,logfile,1)

mysql_query = "SELECT id_dataset, api_root, countfield1, countfield2, id_compteur1, id_compteur2, last_record_timestamp, last_update FROM datasets WHERE id_dataset = '"+target_dataset_id+"' LIMIT 1"


try:
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(mysql_query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    dataset_apiroot     = data['api_root'][0]
    dataset_countfield1 = data['countfield1'][0]
    dataset_idcompteur1 = data['id_compteur1'][0]
    dataset_countfield2 = data['countfield2'][0]
    dataset_idcompteur2 = data['id_compteur2'][0]
    dataset_lastrecord  = data['last_record_timestamp'][0]
    dataset_lastupdate  = data['last_update'][0]

    logmessage("Successfully fetched dataset information : ",logfile,2)
    logmessage("  > Dataset API root : "+dataset_apiroot,logfile,1)
except Exception as oops : 
    logmessage("Echec de récupération des métadonnées du dataset '"+target_dataset_id+"' : "+format(oops),logfile,5)
    quit()

if(dataset_lastrecord is None):
    logmessage("  > Dataset latest record : Not set",logfile,1)
else:
    #We don't like naive
    dataset_lastrecord = locale_tz.localize(dataset_lastrecord)    
    logmessage("  > Dataset latest record : "+datetime.strftime(dataset_lastrecord,"%d/%m/%Y %H:%M:%S%z"),logfile,1)
logmessage("  > Dataset last update : "+datetime.strftime(dataset_lastupdate,"%d/%m/%Y %H:%M:%S%z"),logfile,1)
if(dataset_idcompteur2 is None):
    two_ways_record = False
    logmessage("  > Count field : "+dataset_countfield1,logfile,1)
    logmessage("  > Counter ID  : "+dataset_idcompteur1,logfile,1)
else:
    logmessage("  > Count field (way 1) : "+dataset_countfield1,logfile,1)
    logmessage("  > Counter ID  (way 1) : "+dataset_idcompteur1,logfile,1)
    logmessage("  > Count field (way 2) : "+dataset_countfield2,logfile,1)
    logmessage("  > Counter ID  (way 2) : "+dataset_idcompteur2,logfile,1)

#########################################################################################################################################
# CORE PROCESSING FETCHES DATA FOR EACH DATE AND ADDS TO DB
#########################################################################################################################################
target_date = start_date
date_ok = 0
date_fail = 0
date_count = 0

while(target_date <= end_date):
    target_date_str = datetime.strftime(target_date,"%d/%m/%Y")
    date_count += 1
    logmessage("Retrieving data for date "+target_date_str,logfile,1)

##############################################
# QUERYING API

    uri = (
        dataset_apiroot
        + "rows=10000&facet=date"
        + "&refine.date="+datetime.strftime(target_date,"%Y-%m-%d")
        + "&timezone=Europe%2FParis&scdsqcvsvfrdvfff"
    )

    headers = {
        "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
        "Accept-Encoding": "gzip, deflate, br",
        "DNT": "1",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "cross-site"
    }

    logmessage("   ["+target_date_str+"] Fetching data from URI "+uri,logfile,1)

    chronostart = datetime.now()
    response = requests.get(url=uri, headers=headers)
    chronostop = datetime.now()
    chronotime = chronostop-chronostart
    if response.status_code == 200:
        logmessage("   ["+target_date_str+"] Successfully retrieved data, "+str(len(response.content))+" bytes in "+str(chronotime.total_seconds())+"s",logfile,2)
    elif response.status_code > 500:
        logmessage("   ["+target_date_str+"] A server error occured, stopping processing of dataset",logfile,5)
        logmessage("   ["+target_date_str+"] Error : "+str(response.status_code)+" "+response.reason,logfile,4)
        #exits the loop
        date_fail +=1
        break
    elif response.status_code > 400:
        logmessage("   ["+target_date_str+"] An request error occured, stopping processing of dataset",logfile,5)
        logmessage("   ["+target_date_str+"] Error : "+str(response.status_code)+" "+response.reason,logfile,4)
        #exits the loop
        date_fail +=1
        break
    else:
        logmessage("   ["+target_date_str+"] Could not retrieve data from API. Error : "+ str(response.status_code),logfile,4)
        logmessage("   ["+target_date_str+"]    "+response.reason,logfile,4)
    
##############################################
# INTERPRETING JSON

    try:
        logmessage("   ["+target_date_str+"] Extracting data from JSON response",logfile,1)
        data = json.loads(response.content)
        logmessage("   ["+target_date_str+"] "+str(len(data["records"]))+" records in set",logfile,1)
        
    except Exception as oops:
        logmessage("   ["+target_date_str+"] Could not load JSON data : "+format(oops),logfile,4)


##############################################
# ADDS RECORDS TO SQL QUERY

    record_count  = len(data["records"])

    #Initiate MySQL data list 
    mysql_params = []
    item_number = 0
    #Loops through records
    logmessage("   ["+target_date_str+"] Preparing data for SQL query ("+str(record_count)+" records)",logfile,1)
    for record in data["records"]:

        try:
            item_number += 1 
            new_recordid = record["recordid"]
            new_recordtimestamp = record["record_timestamp"]
            tmp_date = record["fields"]["date"]
            new_date = datetime.strptime(tmp_date,"%Y-%m-%dT%H:%M:%S%z")
            new_count1 = record["fields"][dataset_countfield1]
            new_checksum1 = hashlib.md5((dataset_idcompteur1+datetime.strftime(new_date,"%Y%m%d%H%M%S")).encode('utf-8')).hexdigest()
            if(two_ways_record):
                item_number += 1 
                new_id1 = new_recordid+"1"
                new_id2 = new_recordid+"2"
                new_count2 = record["fields"][dataset_countfield2]
                new_checksum2 = hashlib.md5((dataset_idcompteur2+datetime.strftime(new_date,"%Y%m%d%H%M%S")).encode('utf-8')).hexdigest()
            else:
                new_id1 = new_recordid+"0"

            mysql_params.append({"id_record":new_recordid,"date_time":new_date,"comptage":new_count1,"id_compteur":dataset_idcompteur1,"id_dataset":target_dataset_id,"record_timestamp":new_recordtimestamp,"checksum":new_checksum1 })
            if(two_ways_record):
                mysql_params.append({"id_record":new_recordid,"date_time":new_date,"comptage":new_count2,"id_compteur":dataset_idcompteur2,"id_dataset":target_dataset_id,"record_timestamp":new_recordtimestamp,"checksum":new_checksum2 })
        except KeyError as oups:
            logmessage ("   ["+target_date_str+"] The field "+format(oups)+" could not be found in record "+str(record),logfile,4)

    logmessage("   ["+target_date_str+"] "+str(item_number)+" rows from "+str(record_count)+" records to be added to database",logfile,1)

##############################################
# INSERTING IN DATABASE  
#  
    if(record_count > 0):
        logmessage("   ["+target_date_str+"] Inserting "+str(item_number)+" rows into database",logfile,1)
        mysql_query  = "INSERT INTO comptages (id_record,date_time,comptage,id_compteur,id_dataset,record_timestamp,checksum) VALUES (:id_record,:date_time,:comptage,:id_compteur,:id_dataset,:record_timestamp,:checksum);"
        chronostart = datetime.now()
        try:
            sqlEngine    = create_engine(sql_connect_string_rw, pool_recycle=3600)
            dbConnection = sqlEngine.connect()
            dbConnection.execute(text(mysql_query),mysql_params)
            dbConnection.commit()
            dbConnection.close()
            sqlEngine.dispose()

            chronostop = datetime.now()
            chronotime = chronostop-chronostart
            date_ok += 1
            logmessage("   ["+target_date_str+"] Successfuly inserted "+str(item_number)+" rows into database in "+str(chronotime.total_seconds())+"s",logfile,2)

            #Latest date for metadata
            if(new_date > latest_timestamp):
                latest_timestamp = new_date

        except Exception as oops:
            date_fail +=1
            logmessage("   ["+target_date_str+"] An error occured during insertion : "+format(oops),logfile,4) 
    else:
        logmessage("   ["+target_date_str+"] Record list for that date is empty",logfile,3)
        date_fail +=1

    target_date = target_date + timedelta(days=1)


##############################################
# UPDATES DATASET METADATA
logmessage("Updating dataset metadata if necessary",logfile,1)

current_recordtimestamp = dataset_lastrecord
if(current_recordtimestamp is None):
    current_recordtimestamp = datetime(1970,1,1,0,0,0,0,pytz.UTC)

if(current_recordtimestamp < latest_timestamp):
    current_recordtimestamp_str = datetime.strftime(current_recordtimestamp,"%d/%m/%Y %H:%M:%S%z")
    latest_timestamp_str = datetime.strftime(latest_timestamp,"%d/%m/%Y %H:%M:%S%z")
    logmessage("Added data ("+latest_timestamp_str+") is more recent than previous ("+current_recordtimestamp_str+")",logfile,1)

    mysql_query  = "UPDATE datasets SET last_record_timestamp=(:a) WHERE id_dataset=(:b)"
    mysql_params = [{"a":latest_timestamp,"b":target_dataset_id}]

    try:
        sqlEngine    = create_engine(sql_connect_string_rw, pool_recycle=3600)
        dbConnection = sqlEngine.connect()
        dbConnection.execute(text(mysql_query),mysql_params)
        dbConnection.close()
        sqlEngine.dispose()
        logmessage("Successfully updated dataset metadata",logfile,2)

    except Exception as oops:
        logmessage("Could not update dataset metadata : "+format(oops),logfile,4) 


##############################################
# SCRIPT END


if(date_ok == 0):
    report_level = 4
elif(date_fail > 0):
    report_level = 3
else:
    report_level = 2
global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,report_level)
logmessage("Script ending after "+exectimestr,logfile,report_level)
logmessage("   Inserted data for "+str(date_ok)+" dates over "+str(date_count)+" processed ("+str(date_fail)+" failure(s))",logfile,report_level)


