#!/usr/bin/python3

##########################################################################################################################################
# IMPORTS
import argparse

import locale
from datetime import datetime, date
from time import strftime
from dateutil.relativedelta import relativedelta
import pytz
import urllib.parse
import re

from mastodon import Mastodon

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import seaborn; seaborn.set()

#Importing config file and functions
import xxxx_stats_config as config
from xxxx_stats_functions import *

##########################################################################################################################################
# SETTINGS AND INIT

locale.setlocale(locale.LC_TIME,'fr_FR.utf8')
locale_tz = pytz.timezone("Europe/Paris")

logfile = None
two_ways_record = True 

#Default date
default_date = datetime.strftime((datetime.now() - relativedelta(months=1)).replace(day=1),"%d/%m/%Y")

#Pixel size
px = 1/plt.rcParams['figure.dpi']

#From config file
EVO_TOLERANCE = config.EVO_TOLERANCE
MAX_TOOT_SIZE = config.MAX_TOOT_SIZE
MAX_COUNTERS = config.MAX_COUNTERS_MS

MS_API_URL = config.PRS_MS_API_URL
MS_API_TOKEN = config.PRS_MS_API_TOKEN
IMG_TEMPDIR = config.IMG_TEMPDIR
GRAPH_HEIGHT = config.GRAPH_HEIGHT
GRAPH_WIDTH = config.GRAPH_WIDTH
GRAPH_TITLE_FONTSIZE = config.GRAPH_TITLE_FONTSIZE
GRAPH_AXES_FONTSIZE = config.GRAPH_AXES_FONTSIZE
CITY_LAT = config.PRS_CITY_LAT
CITY_LONG = config.PRS_CITY_LONG
WEATHER_API_ROOT  = config.WEATHER_API_ROOT
WEATHER_ARCHIVE_API_ROOT = config.WEATHER_ARCHIVE_API_ROOT
DAY_START = config.DAY_START
DAY_END   = config.DAY_END
DAY_RAIN_LIMIT = config.DAY_RAIN_LIMIT
HOUR_RAIN_LIMIT = config.HOUR_RAIN_LIMIT
CEIL_PRECIPITATION = config.CEIL_PRECIPITATION
PRECIP_GRADIENT_LOW = config.PRECIP_GRADIENT_LOW
PRECIP_GRADIENT_HIGH = config.PRECIP_GRADIENT_HIGH
WEATHER_API_MAX_MONTHS = config.WEATHER_API_MAX_MONTHS

NB_COUNTERS_TOP_EVO = config.NB_COUNTERS_TOP_EVO
EVO_ABNORMAL_OVER = config.EVO_ABNORMAL_OVER

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Let's go!")

logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* paris-stats-monthly-toot.py                                                        *",logfile,2)
logmessage("*    Toots monthly about bike counter statistics of Paris                            *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*   Version 1.0 - 18/03/2023 - Initial version                                       *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1)
global_chronostart = datetime.now()

##########################################################################################################################################
# INTERPRETING ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--date'  ,'-d', help="Date dans le mois pour lequel on traite les statistiques. Par défaut le premier jour du mois précédent", type= str, default=default_date)
parser.add_argument('--tootit','-t', help="Switch pour indiquer que l'on pouette le résultat",action='store_true', default=False)
args = parser.parse_args()


#Date is an actual date
try:
    target_date = datetime.strptime(args.date,"%d/%m/%Y").replace(day=1)
except Exception as oups:
    logmessage("La date fournie '"+args.date+"' est invalide",logfile,5)
    print(parser.format_help())
    quit()  

#Date is in the past
target_date_age = datetime.now() - target_date 
if(target_date_age.total_seconds() < 0):
    logmessage("La date fournie '"+args.date+"' est dans le futur",logfile,5)
    quit()

logmessage("Date : "+datetime.strftime(target_date,"%d-%m-%Y"),logfile,1)

#Do we toot or not?
toot_it = args.tootit
if(toot_it):
    logmessage("Mode TootIt actif : le resultat sera publié dans un pouet",logfile,2)
else:
    logmessage("Mode TootIt inactif : le resultat ne sera pas poueté",logfile,3)

#Initialisation de l'objet mastodon
if(toot_it):
    logmessage("Initialisation de l'objet Mastodon",logfile,1)
    mastodon = Mastodon(access_token = MS_API_TOKEN ,api_base_url=MS_API_URL)


###
# INFORMATIONS DE PLUVIOMETRIE ET DE TEMPERATURE
####################################################################################################################################################################################
logmessage("Compilation des informations de pluviométrie et de température",logfile,1)

#API classique ou archive
if(relativedelta(date.today(),target_date).months>=WEATHER_API_MAX_MONTHS):
    logmessage("  La date cible est à plus de "+str(WEATHER_API_MAX_MONTHS)+" mois ("+str(relativedelta(target_date,date.today()).months)+"), on doit utiliser l'API d'archive",logfile,3)
    WEATHER_API_ROOT = WEATHER_ARCHIVE_API_ROOT

#Données de température et de pluviométrie de la journée
logmessage("  Récupération des données de température et de pluviométrie de la journée",logfile,1)
start_date_str= datetime.strftime(target_date,"%Y-%m-%d")
end_date_str = datetime.strftime(target_date + relativedelta(months=+1) - relativedelta(days=+1),"%Y-%m-%d")

meteo_url = WEATHER_API_ROOT+"latitude="+CITY_LAT+"&longitude="+CITY_LONG+"&timezone="+urllib.parse.quote(str(locale_tz.zone), safe='')+"&start_date="+start_date_str+"&end_date="+end_date_str+"&daily=temperature_2m_max,temperature_2m_min,precipitation_sum&format=csv"
logmessage("  Appel de : "+meteo_url,logfile,1)
try:
    daily_weather=pd.read_csv(meteo_url,skiprows=2)
    err_meteo = False
    logmessage("  OK : "+str(len(daily_weather))+" lignes récupérées",logfile,2)
except Exception as oops:
    logmessage("  Echec de la requête, les données météo ne seront pas intégrées : "+format(oops),logfile,4)
    err_meteo = True

if (not(err_meteo)):
    #Pluviométrie du mois
    month_total_rain = round(np.sum(daily_weather['precipitation_sum (mm)']),1)
    month_total_raindays = len(daily_weather[daily_weather['precipitation_sum (mm)']>=DAY_RAIN_LIMIT]['precipitation_sum (mm)'])
    logmessage("   Pluviométrie : "+str(month_total_rain)+"mm sur "+str(month_total_raindays)+" jours",logfile,1)

###
# COMPTAGES DU MOIS, TOP 5 ET EVOLUTION SUR UN AN
####################################################################################################################################################################################

# Nombre de passage par compteur
logmessage("Base de données : nombres de passages dans le mois par compteur pour le top "+str(MAX_COUNTERS),logfile,1)

try:
    month_counts= sql_get_city_global_monthcount(target_date,"Paris",MAX_COUNTERS)
    if(len(month_counts)==0):
        logmessage("Pas de comptages pour cette date en base de données",logfile,4)
        quit()
    logmessage("  OK : "+str(len(month_counts))+" enregistrements trouvés",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Total du mois
logmessage("Base de données : total du mois",logfile,1)
try:
    total_mois = sql_get_city_total_monthcount(target_date,"Paris")
    logmessage("   Total: "+str(total_mois),logfile,1)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()


###
# LISTE DES SITES DE COMPTAGE
####################################################################################################################################################################################
logmessage("Reconstruction de liste des sites de comptage dont les compteurs sont actifs (simplifiés en 'compteurs' par la suite)",logfile,1)

compteurs = pd.DataFrame(month_counts.index)


###
# PLACE DU MOIS DANS LE TOP 5
#############################
logmessage("Recupère le rang du mois s'il est dans les 5 premiers",logfile,1)

try:
    month_ranking = sql_get_city_month_ranking("Paris",count=5)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

try:
    month_rank = int(month_ranking.loc[datetime.strftime(target_date,"%Y-%m")]['rank'])
    logmessage("  Le mois "+datetime.strftime(target_date,"%Y-%m")+" est le "+str(month_rank)+"er/ème meilleur mois",logfile,1)
except KeyError:
    month_rank = 99
    logmessage("  Le mois "+datetime.strftime(target_date,"%Y-%m")+" n'est pas dans les 5 meilleurs mois",logfile,1)

if(month_rank == 1):
    record_ville = True
else:
    record_ville = False


###
# COMPTAGES DU MOIS DE L'ANNÉE PRECEDENTE
#########################################
target_date_1yb = target_date - relativedelta(years=1)

logmessage("Récupération du comptage du même mois l'année précédente par compteur",logfile,1)
try:
    previous_year_counts =  sql_get_city_global_monthcount(target_date_1yb,"Paris",count_field_name="comptage_1yb",counters=month_counts.index)
    logmessage("  OK : "+str(len(previous_year_counts))+" enregistrements calculés",logfile,2)
    
    #Joins the result to the month count DataFrame
    month_counts = pd.concat([month_counts, previous_year_counts], axis = 1)

except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Calcul du total du mois de l'année précédente
logmessage("Récupération du total du même mois de l'année précédente",logfile,1)
try:
    total_mois_1yb = sql_get_city_total_monthcount(target_date_1yb,"Paris")
    logmessage("   Total: "+str(total_mois_1yb),logfile,1)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

###
# CALCUL EVOLUTION 1 AN
#######################

#Evolution par compteur
logmessage("Calcul de l'évolution sur un an par compteur",logfile,1)

#Evolution en décimale, formatée, en emoji
month_counts['evo'] = month_counts.apply(df_add_monthevo_column, axis = 1)
month_counts['evotxt'] = month_counts.apply(df_add_monthevotxt_column, axis = 1)
month_counts['evoemoji'] = month_counts.apply(df_add_monthevoemoji_column, axis = 1)

#Evolution globale
logmessage("Calcul de l'évolution sur un an globale",logfile,1)
month_evo_total = (total_mois - total_mois_1yb)/total_mois_1yb
if(month_evo_total > 0):
    evo_sign = "+"
else:
    evo_sign = ""

#Emoji
if(month_evo_total > EVO_TOLERANCE):
    month_evo_total_emoji = emoji.INCR
elif(month_evo_total < -EVO_TOLERANCE):
    month_evo_total_emoji = emoji.DECR
else:
    month_evo_total_emoji = emoji.STABLE

month_evo_total_str = evo_sign+str(round(month_evo_total*100,1))+"%"

logmessage("   Evolution globale: "+month_evo_total_str,logfile,1)

###
# MEILLEUR JOUR
####################################################################################################################################################################################
logmessage("Meilleure journée",logfile,1)

try:
    bestday =  sql_get_city_bestmonthday(target_date,"Paris")
    logmessage("  "+datetime.strftime(bestday['date'][0],"%A %e")+" avec "+str(bestday['total'][0]),logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

###
# MOYENNE PAR JOUR ET EVOLUTION
####################################################################################################################################################################################

#Moyenne par jour
logmessage("Calcul de la moyenne journalière",logfile,1)
day_average = int(round(total_mois / (((target_date + relativedelta(months=+1)) - target_date ).days),0))
logmessage("   "+str(day_average)+" passages par jours en moyenne",logfile,1)

#Moyenne par jour de l'année précédente
logmessage("Calcul de la moyenne journalière de l'année précédente",logfile,1)
day_average_1yb = int(round(total_mois_1yb / (((target_date_1yb + relativedelta(months=+1)) - target_date_1yb ).days),0))
logmessage("   "+str(day_average_1yb)+" passages par jours en moyenne",logfile,1)


#Moyenne par jour par compteur
logmessage("Calcul de la moyenne journalière par compteur",logfile,1)
month_counts['target_date'] = target_date #required to be able to calculate average
month_counts['moyenne'] = month_counts.apply(df_add_dayaverage_column, axis = 1)


###
# TOP N DES EVOLUTIONS DE COMPTEURS
####################################################################################################################################################################################
logmessage("TOP "+str(NB_COUNTERS_TOP_EVO)+" des évolutions de compteurs",logfile,1)

try:
    logmessage("  Base de données : comptages de tous les compteurs",logfile,1)
    all_counters_count = sql_get_city_global_monthcount(target_date,"Paris")
    logmessage("  OK, "+str(len(all_counters_count))+" totaux de compteurs récupérés",logfile,2)

    logmessage("  Base de données : comptages de l'année précédente de tous les compteurs",logfile,1)
    all_counters_total_1yb =  sql_get_city_global_monthcount(target_date_1yb,"Paris",count_field_name="comptage_1yb")
    logmessage("  OK, "+str(len(all_counters_total_1yb))+" totaux de compteurs récupérés",logfile,2)
    err_top_n = False
except Exception as oops:
    logmessage("  Echec de la requête: "+format(oops),logfile,5)
    err_top_n = True

if(not(err_top_n)):
    #jonction des deux DF
    logmessage("  Jonction des dataframes de l'année N et N-1",logfile,1)
    all_counters_evo = pd.concat([all_counters_count, all_counters_total_1yb], axis = 1)

    #Evolution en décimale, formatée, en emoji
    logmessage("  Calcul de l'évolution par ligne",logfile,1)
    all_counters_evo['evo'] = all_counters_evo.apply(df_add_monthevo_column, axis = 1)
    all_counters_evo['evotxt'] = all_counters_evo.apply(df_add_monthevotxt_column, axis = 1)
    all_counters_evo['evoemoji'] = all_counters_evo.apply(df_add_monthevoemoji_column, axis = 1)
    
    #Tri et filtrage
    logmessage("  Tri et filtrage des évolutions",logfile,1)
    top_counters_evo = all_counters_evo[all_counters_evo['evo']<EVO_ABNORMAL_OVER].sort_values(by=['evo'],axis=0,ascending=False)[:NB_COUNTERS_TOP_EVO]
    top_counters_evo = top_counters_evo[top_counters_evo['evo']>0]

    logmessage("  TOP "+str(NB_COUNTERS_TOP_EVO)+":",logfile,1)
    for i in top_counters_evo.reset_index()['nom_axe_abrg']:
        logmessage("    >"+i+" : "+str(top_counters_evo['evotxt'][i]),logfile,1)

###
# GRAPHIQUES
####################################################################################################################################################################################


###
# GRAPH DU TRAFIC PAR JOUR ET SA MMC 7 JOURS
#############################################
logmessage("Génération du graph de l'historique sur le mois",logfile,1)

try:
    #récupération des données
    historique_mois = sql_get_city_history(target_date,"Paris",(((target_date + relativedelta(months=+1)) - target_date ).days),3)
    logmessage("  "+str(len(historique_mois))+" enregistrements récupérés",logfile,2)

    #ajout de la MMC7j
    logmessage("  Ajout de la MMC7J au dataframe",logfile,1)
    for i in range(0,historique_mois.shape[0]-6):
        historique_mois.loc[historique_mois.index[i+3],'MMC7'] = np.round(((historique_mois.iloc[i,1]+ historique_mois.iloc[i+1,1] +historique_mois.iloc[i+2,1]+historique_mois.iloc[i+3,1]+historique_mois.iloc[i+4,1]+historique_mois.iloc[i+5,1]+historique_mois.iloc[i+6,1])/7),1)
    #Suppression des 3 premières lignes et des trois dernières
    historique_mois = historique_mois.iloc[3:-3]
    historique_mois = historique_mois.reset_index()

    if(not(err_meteo)):
        historique_mois['temp_max'] = daily_weather['temperature_2m_max (°C)']
        historique_mois['temp_min'] = daily_weather['temperature_2m_min (°C)']

    err_ghm = False
except Exception as oops:
    logmessage("  Impossible de récupérer les données pour le graphique : "+format(oops),logfile,4)
    err_ghm = True

if(not(err_ghm)):
    logmessage("  Génération du graphique",logfile,1)
    #Tableau du dégradé
    nb_steps = int(CEIL_PRECIPITATION / 0.1)+1
    gradient = get_color_gradient(PRECIP_GRADIENT_LOW,PRECIP_GRADIENT_HIGH,nb_steps)

    #mapping de la pluviometrie en dégradé
    colormap = []
    for dayrain in daily_weather['precipitation_sum (mm)']:
        if(dayrain > CEIL_PRECIPITATION):
            index = int(CEIL_PRECIPITATION*10)
        else:
            index =  int(round(dayrain*10,0))
        colormap.append(gradient[index])

    #limite supérieur des ordonnées selon le max dans le dataframe
    top_limit = int(math.ceil((historique_mois.max(0)['total']+(historique_mois.max(0)['total']/5)) / 1000.0)) * 1000

    #Creation du graphique
    fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)

    #Bornes de l'axe des ordonnées
    plt.ylim(bottom=0,top=top_limit)

    #Formatage des règles et de la guille
    plt.rcParams['xtick.major.size'] = 5
    plt.rcParams['xtick.major.width'] = 2
    plt.rcParams['xtick.bottom'] = True
    plt.rcParams['ytick.left'] = True
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mdates.MO, interval=1))
    ax.xaxis.set_minor_locator(mdates.DayLocator())
    ax.yaxis.set_minor_locator(ticker.MultipleLocator(10000))
    #grille
    ax.grid(visible=True, which='major', color='w', linewidth=1.0)
    ax.grid(visible=True, which='minor', color='w', linewidth=0.5)

    #Tracés
    ax.plot('date', 'MMC7', data=historique_mois, linewidth=3, color=[0.267, 0, 0.149])
    ax.bar(historique_mois['date'], historique_mois['total'],color=colormap)

    ax2 = ax.twinx()
    ax2.plot('date', 'temp_max', data=historique_mois, linewidth=2, color='red',linestyle='dashed')
    ax2.plot('date', 'temp_min', data=historique_mois, linewidth=2, color='blue',linestyle='dashed')
    ax2.grid(visible=False)

    plt.ylim(
        bottom=math.floor(math.floor(np.min(historique_mois['temp_min'])/5))*5,
        top=math.ceil(math.ceil(np.max(historique_mois['temp_max'])*2)/5)*5)

    #Mise en forme des étiquettes
    ax.set_ylabel('Nombre de passages',fontsize=GRAPH_AXES_FONTSIZE)
    ax2.set_ylabel('Températures min/max',color='grey',loc="bottom",fontsize=GRAPH_AXES_FONTSIZE)
    ax2.tick_params(axis='y', colors='white')
    ax.set_xlabel('Dégradé fonction de la pluviometrie',fontdict={'fontsize':GRAPH_AXES_FONTSIZE-2})
    ax.set_title('Évolution du trafic sur le mois de '+datetime.strftime(target_date,"%B %Y"), fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%a %e %b'))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter('%a'))

    for label in ax2.get_yticklabels():
        if(int(re.sub(u"\u2212", "-", label.get_text())) <= math.ceil(math.ceil(np.max(historique_mois['temp_max']))/5)*5):
            label.set(color='grey',fontsize=GRAPH_AXES_FONTSIZE-4)

    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right',fontsize=GRAPH_AXES_FONTSIZE)
    for label in ax.get_xticklabels(which='minor'):
        label.set(rotation=30, horizontalalignment='right',fontsize=GRAPH_AXES_FONTSIZE-2)

    #Enregistrement de l'image
    ghm_img_filepath = IMG_TEMPDIR+"ms_graph-historique-mois_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("  Enregistrement de l'image en tant que "+ghm_img_filepath,logfile,1)
    try:
        plt.savefig(ghm_img_filepath)
        logmessage("  Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("  Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_ghm = True


###
# GRAPH DE LA COMPARAISON ANNUELLE
############################################################
logmessage("Génération du graph de comparaison annuelle",logfile,1)


#Recuperation des données du graphique
try:
    historiques_annuels = sql_get_city_yearhistory(target_date,datetime(2020,1,1),"Paris",3,False)
    logmessage("  OK : "+str(len(historiques_annuels))+" enregistrements calculés",logfile,2)
    err_gca = False
except Exception as oops:
    logmessage("   Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_gca = True

if(not(err_gca)):
    #préparation des données
    for i in range(0,historiques_annuels.shape[0]-6):
        historiques_annuels.loc[historiques_annuels.index[i+3],'MMC7'] = np.round(((historiques_annuels.iloc[i,2]+ historiques_annuels.iloc[i+1,2] +historiques_annuels.iloc[i+2,2]+historiques_annuels.iloc[i+3,2]+historiques_annuels.iloc[i+4,2]+historiques_annuels.iloc[i+5,2]+historiques_annuels.iloc[i+6,2])/7),1)
    historiques_annuels = historiques_annuels[3:]
    
    data_annees = historiques_annuels.pivot_table('MMC7', index='jour', columns='annee')
    data_annees = data_annees.reset_index()

    columns_temp = []
    for i in data_annees.columns:
        columns_temp.append(str(i))
    data_annees.columns=columns_temp

    #couleurs
    line_colors = [
        [1,0.733,0.494],
        [1,0.663,0.365],
        [0.824,0.506,0.22],
        [0.49,0.275,0.082],
        [0.192,0.11,0.035]
    ]

    #graphique
    fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)
   
    if(len(data_annees.columns)<6): #Si on a moins de 5 années d'historique
        first_year = 1
    else:
        first_year = len(data_annees.columns)-5
    i=0
    for year in data_annees.columns[first_year:len(data_annees.columns)]:
        ax.plot('jour', year, data=data_annees, linewidth=3, color=line_colors[i])
        i+=1

    # Major ticks every month, minor ticks every week,
    ax.xaxis.set_major_locator(mdates.MonthLocator())
    #grille
    ax.grid(visible=True, which='major', color='w', linewidth=1.0)
    ax.grid(visible=True, which='minor', color='w', linewidth=0.5)
    ax.set_ylabel('Nombre de passages', fontsize=GRAPH_AXES_FONTSIZE)
    ax.legend(fontsize=GRAPH_AXES_FONTSIZE)
    ax.set_title('Comparaison du trafic vélo annuel', fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})

    ax.xaxis.set_major_formatter(mdates.DateFormatter('    %b'))
    # Rotates and right-aligns the x labels so they don't crowd each other.
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=0, horizontalalignment='left', fontsize=GRAPH_AXES_FONTSIZE)
    for label in ax.get_yticklabels(which='major'):
        label.set(fontsize=GRAPH_AXES_FONTSIZE-4)

    #Enregistrement de l'image
    gca_img_filepath = IMG_TEMPDIR+"ms_graph-historiques-annees_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("  Enregistrement de l'image en tant que "+gca_img_filepath,logfile,1)
    try:
        plt.savefig(gca_img_filepath)
        logmessage("  Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("  Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_gca = True


###
# GRAPH DU TRAFFIC PAR COMPTEUR
############################################################
logmessage("Génération du graph trafic par compteur",logfile,1)

#Recuperation des données du graphique
try:
    historique_mois_compteurs = sql_get_city_historycompteurs(target_date,"Paris",(((target_date + relativedelta(months=+1)) - target_date ).days),0,False,month_counts.index)
    logmessage("  OK : "+str(len(historiques_annuels))+" enregistrements calculés",logfile,2)
    err_gtpc = False
except Exception as oops:
    logmessage("   Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_gtpc = True

if(not(err_gtpc)):
    #Reassemblage des données
    dates = pd.DataFrame({'date':pd.date_range(target_date,periods=(((target_date + relativedelta(months=+1)) - target_date )).days)})
    dates = dates.set_index('date')
    for cpt in compteurs['nom_axe_abrg']:
        tmpdf = pd.DataFrame(historique_mois_compteurs.loc[historique_mois_compteurs['compteur']==cpt])
        tmpdf = tmpdf.rename(columns={"total": cpt})
        tmpdf = tmpdf.drop(columns = ['compteur'])
        tmpdf = tmpdf.set_index('date')
        dates = dates.join(tmpdf)
    dates = dates.reset_index()

    #Couleurs
    couleurs = (
    [0.373, 0.212, 0.161],
    [0.271, 0.596, 0.431],
    [0.384, 0.298, 0.584],
    [0.557, 0.459, 0.78],
    [0.851, 0.498, 0.388],
    [0.851, 0.784, 0.388]
    )

    #Graphique
    fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)

    #Ligne pour chaque compteur
    i=0
    for cpt in compteurs['nom_axe_abrg']:
        ax.plot('date', cpt,'', data=dates, linewidth=3, color=couleurs[i])
        i+=1

    #Bornes des ordonnées
    top_limit = int(math.ceil((dates.iloc[:,1:len(compteurs)+1].max().max()+(dates.iloc[:,1:len(compteurs)+1].max().max()/10)) / 1000.0)) * 1000
    plt.ylim(bottom=0)
    plt.ylim(top=top_limit)

    #echelles
    plt.rcParams['xtick.major.size'] = 5
    plt.rcParams['xtick.major.width'] = 2
    plt.rcParams['xtick.bottom'] = True
    plt.rcParams['ytick.left'] = True
    ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=mdates.MO, interval=1))
    ax.xaxis.set_minor_locator(mdates.DayLocator())
    ax.yaxis.set_minor_locator(ticker.MultipleLocator(250))
    
    #grille
    ax.grid(visible=True, which='major', color='w', linewidth=1.0)
    ax.grid(visible=True, which='minor', color='w', linewidth=0.5)

    #Legende
    plt.legend(fontsize=GRAPH_AXES_FONTSIZE)

    #Etiquettes    
    ax.set_ylabel('Nombre de passages',fontdict={'fontsize':GRAPH_AXES_FONTSIZE})
    ax.set_title('Evolution du trafic par compteur sur le mois de '+datetime.strftime(target_date,"%B %Y"), fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%a %e %b'))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter('%a'))
    # Rotates and right-aligns the x labels so they don't crowd each other.
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right',fontsize=GRAPH_AXES_FONTSIZE)

    for label in ax.get_xticklabels(which='minor'):
        label.set(rotation=30, horizontalalignment='right',fontsize=GRAPH_AXES_FONTSIZE-4)

    #Enregistrement de l'image
    gtpc_img_filepath = IMG_TEMPDIR+"ms_graph_historique-compteurs_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("Enregistrement de l'image en tant que "+gtpc_img_filepath,logfile,1)
    try:
        plt.savefig(gtpc_img_filepath)
        logmessage(" Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_grh = True



###
# POUET PRINCIPAL
############################################################
logmessage("Preparation du premier pouet",logfile,1)

#Ligne d'introduction
logmessage(" Introduction",logfile,1)
toot_text  = "#Paris: Résumé du trafic vélo de "+datetime.strftime(target_date,"%B %Y")+emoji.CYCLST+"\r\n"

#Comptage et rang du mois
logmessage(" Comptage et rang",logfile,1)
toot_text += strs(int(total_mois))+" passages "
if(record_ville):
    toot_text += ", c'est un record!"+emoji.TADA+"\r\n"
elif(month_rank == 2):
    best_month_str = datetime.strftime(datetime.strptime(month_ranking.reset_index()['mois'][0],"%Y-%m"),"%B %Y")
    best_month_cnt = strs(int(month_ranking.reset_index()['comptage'][0]))
    toot_text += ", c'est le 2nd meilleur mois après "+best_month_str+" et ses "+best_month_cnt+""+emoji.TADA+"\r\n"
elif(month_rank <= 5):
    toot_text += ", c'est le "+str(month_rank)+"ème meilleur mois!\r\n"

#Evolution
logmessage(" Evolution",logfile,1)
toot_text += month_evo_total_emoji+" "+month_evo_total_str+" par rapport à "+datetime.strftime(target_date_1yb,"%Y")+"\r\n"

#Moyenne par jour
logmessage(" Moyenne par jour",logfile,1)
toot_text += "soit une moyenne de "+strs(int(day_average))+" passages/jour contre "+strs(int(day_average_1yb))+" en "+datetime.strftime(target_date_1yb,"%Y")+"\r\n"

#Meilleur journée
logmessage(" Meilleure journée",logfile,1)
if(int(datetime.strftime(bestday['date'][0],"%e"))==1):
    er = "er"
else:
    er = ""

toot_text += "La meilleure journée a été le "+datetime.strftime(bestday['date'][0],"%A")+" "+datetime.strftime(bestday['date'][0],"%e").lstrip()+er+" avec "+strs((int(bestday['total'][0])))+" passages\r\n"

#Pluviométrie
if(not(err_meteo)):
    if(month_total_raindays == 0):
        toot_text += "À noter, il n'a pas plu de tout le mois.\r\n"
    elif(month_total_raindays ==1 ):
        toot_text += "À noter, il n'a plu une seule journée, "+str(month_total_rain)+"mm.\r\n"
    else:
        toot_text += "À noter, il y a eu "+str(month_total_raindays)+" jours de pluie, pour "+str(month_total_rain)+"mm au total.\r\n"

#Annonce de la suite
toot_text += emoji.DOWN

#Resumé du toot : 
logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
    #envoi du pouet
    if(toot_it):
        attachment_ids = []
        try:
            #Si on a pu génrer l'image du graph d'historique mensuel
            if(not err_ghm):
                try:
                    logmessage("Envoi de l'image "+ghm_img_filepath,logfile,1)
                    attachment_alt = "Graphique des comptages de passages de vélos du mois de "+datetime.strftime(target_date,"%B %Y")+" avec des informations de pluviométrie et de températures min/max"
                    attachment = mastodon.media_post(media_file=ghm_img_filepath,description=attachment_alt)
                    attachment_id = str(attachment.id)
                    logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                    attachment_ids.append(attachment_id)
                except Exception as oops:
                    logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text,media_ids=attachment_ids)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"

###
# POUET DE REPONSE AVEC LE DETAIL PAR COMPTEUR
############################################################
logmessage("Préparation du pouet de réponse des détails par compteur",logfile,1)
reply_to_toot = toot_id

#Détails par compteur : 
logmessage(" Par compteur",logfile,1)
toot_text = "Pour le top "+str(MAX_COUNTERS)+" des compteurs, celà donne :"
i=0
for compteur in compteurs['nom_axe_abrg']:
    logmessage("   Compteur : "+compteur,logfile,1)
    i+=1
    if(i==1):
        nfo = " par jour"
    else:
        nfo = ""
    if(not str(month_counts['comptage'][compteur]) == 'nan'):
        #Comptage
        compteur_comptage = round(month_counts['comptage'][compteur])
        #Moyenne
        compteur_moyenne = round(month_counts['moyenne'][compteur])
        #Evolution
        compteur_emoji = month_counts['evoemoji'][compteur]
        compteur_evo = "("+compteur_emoji+month_counts['evotxt'][compteur] +")"
        toot_text += "\r\n  "+strs(compteur_comptage)+", soit "+strs(compteur_moyenne)+nfo+" en moyenne "+compteur_evo+" pour "+compteur
    else:
        toot_text += "\r\n"+emoji.NOT+" "+compteur+" indisponible"     

logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
    #envoi du pouet
    if(toot_it):
        attachment_ids = []
        try:
            try:
                logmessage("Envoi de l'image "+gtpc_img_filepath,logfile,1)
                attachment_alt = "Graphique du trafic vélo par compteur sur le mois de "+datetime.strftime(target_date,"%B %Y")+"."
                attachment = mastodon.media_post(media_file=gtpc_img_filepath,description=attachment_alt)
                attachment_id = str(attachment.id)
                logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                attachment_ids.append(attachment_id)
            except Exception as oops:
                logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text, in_reply_to_id=reply_to_toot,media_ids=attachment_ids)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"


###
# POUET DE REPONSE AVEC LE TOP N DES EVOLUTIONS DE COMPTEUR
############################################################
logmessage("Préparation du pouet de réponse du top N des évolutions de compteurs",logfile,1)
if(len(top_counters_evo)>0):
    reply_to_toot = toot_id

    toot_text = "Et voici le top "+str(NB_COUNTERS_TOP_EVO)+" des évolutions de compteurs par rapport à "+datetime.strftime(target_date_1yb,"%Y")

    i=1
    for compteur in top_counters_evo.reset_index()['nom_axe_abrg']:
        match i:
            case 1:
                rank_emoji = emoji.TOP1
            case 2:
                rank_emoji = emoji.TOP2
            case 3:
                rank_emoji = emoji.TOP3
            case _:
                rank_emoji = ""
        compteur_infos = sql_get_compteur_infos(compteur)
        compteur_total = int(top_counters_evo['comptage'][compteur])
        compteur_total_1yb = int(top_counters_evo['comptage_1yb'][compteur])
        compteur_avg = int(round(compteur_total / (((target_date + relativedelta(months=+1)) - target_date ).days),0))
        compteur_avg_1yb = int(round(compteur_total_1yb / (((target_date_1yb + relativedelta(months=+1)) - target_date_1yb).days),0))

        toot_text += "\r\n"+rank_emoji+" "+compteur_infos['nom_axe'][0]+" : "+top_counters_evo['evotxt'][compteur]+" ("+strs(compteur_total)+" contre "+strs(compteur_total_1yb)+", "+strs(compteur_avg)+"/jour contre "+strs(compteur_avg_1yb)+")"
        i+=1
    
    if(i<3):
        toot_text += "\r\n(Oui, ce n'était pas la fête ce mois-ci)"

    logmessage("Pouet : \n\r"+toot_text,logfile,1)

    if(len(toot_text) > MAX_TOOT_SIZE):
        logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
    else:
        logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
        #envoi du pouet
        if(toot_it):
            logmessage("Envoi du pouet",logfile,1)
            try:
                toot = mastodon.status_post(toot_text, in_reply_to_id=reply_to_toot,media_ids=attachment_ids)
                toot_id = toot.id
                logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
            except Exception as oops:
                logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
        else:
            logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
            toot_id = "0000000000000000000"

else:
    logmessage("Aucun compteur n'a d'évolution positive, on se tait donc",logfile,3)

###
# POUET DE REPONSE AVEC LE GRAPH DE COMPARAISON ANNUELLE
############################################################
logmessage("Préparation du pouet de réponse du graphique de comparaison annuelle",logfile,1)
reply_to_toot = toot_id

#Liste des années
target_year = target_date.year

if(len(data_annees.columns)<6): #Si on a moins de 5 années d'historique
    first_year = 1
else:
    first_year = len(data_annees.columns)-5

year_list = ""
for year in data_annees.columns[first_year:len(data_annees.columns)-1]:
    if(year == data_annees.columns[len(data_annees.columns)-2]):
        sep = ""
    elif(year == data_annees.columns[len(data_annees.columns)-3]):
        sep = " et "
    else:
        sep = ", "
    year_list = year_list + str(year) + sep


toot_text = emoji.GRAPH+" Et enfin voici le graphique comparatif des comptages de l'année "+str(target_year)+" par rapport aux années "+year_list
toot_text += "\r\n"+emoji.DOWN

logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
    #envoi du pouet
    if(toot_it):
        attachment_ids = []
        try:
            try:
                logmessage("Envoi de l'image "+gca_img_filepath,logfile,1)
                attachment_alt = "Graphique comparatif des comptages de vélos des années "+str(target_year)+", "+year_list
                attachment = mastodon.media_post(media_file=gca_img_filepath,description=attachment_alt)
                attachment_id = str(attachment.id)
                logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                attachment_ids.append(attachment_id)
            except Exception as oops:
                logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text, in_reply_to_id=reply_to_toot,media_ids=attachment_ids)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"

###
# SCRIPT ENDING
############################################################

global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,1)
logmessage("Script ending after "+exectimestr,logfile,1)