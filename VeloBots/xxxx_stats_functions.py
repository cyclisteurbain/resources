import os
import __main__ as main 
from datetime import datetime
import numpy as np
import pandas as pd
from sqlalchemy import create_engine, text
from dateutil.relativedelta import relativedelta
from datetime import timedelta

#Imports configuration
import xxxx_stats_config as config

#Inits
sql_connect_string_ro = "mysql+pymysql://"+config.MYSQL_RO_USER+":"+config.MYSQL_RO_PASS+"@"+config.MYSQL_HOST+"/"+config.MYSQL_DB
sql_connect_string_rw = "mysql+pymysql://"+config.MYSQL_RW_USER+":"+config.MYSQL_RW_PASS+"@"+config.MYSQL_HOST+"/"+config.MYSQL_DB

#####################################################################################################################################
# GENERAL USE FUNCTIONS AND CLASSES                                                                                                 #
#####################################################################################################################################

##########################################################################
# COLORS : Used for color formating of output
class fgc:
    HEADER  = '\033[95m'
    OKBLUE  = '\033[94m'
    OKGREEN = '\033[92m'
    OKCYAN  = '\033[96m'
    WARNING = '\033[93m'
    FAIL    = '\033[91m'
    ENDC    = '\033[0m'
    BOLD    = '\033[1m'

##########################################################################
# EMOJI : Used for printing emojis
class emoji:
    INCR   = '\U00002197'
    DECR   = '\U00002198'
    STABLE = '\U000027A1'
    DOWN   = '\U00002935'
    NONE   = '\U00002796'
    TADA   = '\U0001F389'
    PEACH  = '\U0001F351'
    NOT    = '\U0000274C'
    WARN   = '\U000026A0'
    CYCLST = '\U0001F6B4'
    BIKE   = '\U0001F6B2'
    GRAPH  = '\U0001F4C8'
    #Weather EMOJIS
    SUNNY  = '\U00002600'
    SUNCLD = '\U0001F324'
    CLOUD  = '\U00002601'
    CLDSUN = '\U000026C5'
    SHOWER = '\U0001F326'
    RAIN   = '\U0001F327'
    SNOW   = '\U0001F328'
    THNDER = '\U0001F329'
    TORNAD = '\U0001F32A'
    FOG    = '\U0001F32B'
    WIND   = '\U0001F32C'
    #Top3
    TOP1   = '\U0001F947'
    TOP2   = '\U0001F948'
    TOP3   = '\U0001F949'

##########################################################################
# logmessage(message,logfile,level) : 
#  Prints a message on stdout and writes it to a log file
#    str message : Message to be printed/written (Mandatory)
#    str logfile : Path to the log file, if not specified, a new log file is created, named after the main script and the current timestamp
#    int level   : error level of the message : 0 for Verbose, 1 for Info, 2 for success, 3 for warning, 4 for error, 5 for fatal error. Default is 9 for debug
#  Returns : str with the path to the log file

def logmessage(message:str,logfile:str=None,level:int=9):

    #Create the logfile if not exists
    if(logfile is None):
        logfile = config.LOGPATH+os.path.basename(main.__file__)+"-"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".log"
        try:
            with open(logfile, 'w') as lf:
                lf.write(os.path.basename(main.__file__)+" : Script startup at "+datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")+"\n")
        except IOError as oups:
            print(fgc.FAIL+"Cannot initiate logfile "+logfile+" : "+format(oups)+fgc.ENDC)

    #Level
    event_time = datetime.strftime(datetime.now(),"%d/%m/%Y %H:%M:%S")
    match level:
        case 0:
            event_prefix = event_time+" [VERBOSE] "
            event_color  = fgc.OKBLUE
        case 1:
            event_prefix = event_time+" [INFO]    "
            event_color  = fgc.OKCYAN
        case 2:
            event_prefix = event_time+" [SUCCESS] "
            event_color  = fgc.OKGREEN
        case 3:
            event_prefix = event_time+" [WARNING] "
            event_color  = fgc.WARNING
        case 4:
            event_prefix = event_time+" [ERROR]   "
            event_color  = fgc.FAIL
        case 5:
            event_prefix = event_time+" [FTERROR] "
            event_color  = fgc.FAIL+fgc.BOLD
        case _:
            event_prefix = event_time+" [DEBUG]   "
            event_color  = ""
    
    #Writes to file
    try:
        with open(logfile, 'a') as lf:
            lf.write(event_prefix+message+"\n")
    except IOError as oups:
        print(fgc.FAIL+"Cannot write to logfile "+logfile+" : "+format(oups)+fgc.ENDC)
    
    #Prints to stdout
    print(event_color+event_prefix+message+fgc.ENDC)
    
    #Returns the path to log file
    return logfile

##########################################################################
# get_weatheremoji(wmocode) : 
#  Retuns a emoji code from emoji class matching wmocode
#  Returns : str with emoji code
def get_weatheremoji(wmo:int):
    #SUNNY
    if(wmo==0):
        return emoji.SUNNY
    #SUNNY WITH CLOUDS
    if(wmo==1):
        return emoji.SUNCLD
    #COULDS WITH SUN
    if(wmo==2):
        return emoji.CLDSUN
    #CLOUDY
    if(wmo==3):
        return emoji.CLOUD
    #RAIN SHOWERS
    if((wmo >= 20 and wmo <= 25) or
        wmo == 27 or
       (wmo >= 80 and wmo <= 82)):
       return emoji.SHOWER
    #RAIN
    if((wmo >= 50 and wmo <= 69) or
       (wmo >= 89 and wmo <= 82)):
       return emoji.RAIN
    #FOGGY
    if((wmo >= 4 and wmo <= 18) or
        wmo == 28 or
       (wmo >= 40 and wmo <= 49)):
        return emoji.FOG
    #TORNADO
    if(wmo == 19):
        return emoji.TORNAD
    #SNOW AND SNOW SHOWERS
    if((wmo >= 36 and wmo <= 39) or
        wmo == 26 or
       (wmo >= 70 and wmo <= 79) or
       (wmo >= 83 and wmo <= 88) or
       (wmo >= 93 and wmo <= 94)):
       return emoji.SNOW
    #THUNDER AND THUNDERSTORMS
    if((wmo >= 95 and wmo <= 99) or
        wmo == 29):
        return emoji.THNDER
    #WIND
    if(wmo >= 30 and wmo <= 35):
        return emoji.WIND
    
    return emoji.SUNNY

#Transform a hex color in RGB
def hex_to_RGB(hex_str):
    return [int(hex_str[i:i+2], 16) for i in range(1,6,2)]

#Creates a gradient from c1 to c2 with n steps
def get_color_gradient(c1, c2, n):
    assert n > 1
    c1_rgb = np.array(hex_to_RGB(c1))/255
    c2_rgb = np.array(hex_to_RGB(c2))/255
    mix_pcts = [x/(n-1) for x in range(n)]
    rgb_colors = [((1-mix)*c1_rgb + (mix*c2_rgb)) for mix in mix_pcts]
    return rgb_colors

#Takes an integer and returns a string with thousand separation
def strs(intstring:int):
    return('{:,}'.format(intstring).replace(',', ' '))


##########################################################################
# df_add_evo_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'mmc' columns and
#   returns a decimal percentage.
#  Returns : float
def df_add_evo_column(row):
    if(not str(row['mmc'])=='nan'):
        evo = (row['comptage'] - row['mmc'])/row['mmc']  
        return evo
    else:
        return np.nan
    

##########################################################################
# df_add_evotxt_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'mmc' columns and
#   returns a formated percentage
#  Returns : str
def df_add_evotxt_column(row):
    if(not str(row['mmc'])=='nan'):
        evo = (row['comptage'] - row['mmc'])/row['mmc']
        if(evo > 0):
            evo_sign = "+"
        else:
            evo_sign = ""
        return evo_sign+str(round(evo*100,1))+"%"
    else:
        return "N/A"

##########################################################################
# df_add_evoemoji_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'mmc' columns and
#   returns the Unicode for the appropriate emoji
#  Returns : str
def df_add_evoemoji_column(row):
    if(not str(row['mmc'])=='nan'):
        evo = (row['comptage'] - row['mmc'])/row['mmc']
        if(evo > config.EVO_TOLERANCE):
            evo_emoji = emoji.INCR
        elif(evo < - config.EVO_TOLERANCE):
            evo_emoji = emoji.DECR
        else:
            evo_emoji = emoji.STABLE
        return evo_emoji
    else:
        return emoji.NONE

##########################################################################
# df_add_evo3jip_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'moyenne3jip' 
#   columns and returns a decimal percentage.
#  Returns : float
def df_add_evo3jip_column(row):
    if(not str(row['moyenne3jip'])=='nan'):
        evo = (row['comptage'] - row['moyenne3jip'])/row['moyenne3jip']
        return evo
    else:
        return np.nan

##########################################################################
# df_add_evo3jiptxt_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'moyenne3jip' 
#   columns and returns a formated percentage
#  Returns : str
def df_add_evo3jiptxt_column(row):
    if(not str(row['moyenne3jip'])=='nan'):
        evo = (row['comptage'] - row['moyenne3jip'])/row['moyenne3jip']
        if(evo > 0):
            evo_sign = "+"
        else:
            evo_sign = ""
        return evo_sign+str(round(evo*100,1))+"%"
    else:
        return "N/A"

##########################################################################
# df_add_evo3jipemoji_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'moyenne3jip' 
#   columns and returns the Unicode for the appropriate emoji
#  Returns : str
def df_add_evo3jipemoji_column(row):
    if(not str(row['moyenne3jip'])=='nan'):
        evo = (row['comptage'] - row['moyenne3jip'])/row['moyenne3jip']
        if(evo > config.EVO_TOLERANCE):
            evo_emoji = emoji.INCR
        elif(evo < - config.EVO_TOLERANCE):
            evo_emoji = emoji.DECR
        else:
            evo_emoji = emoji.STABLE
        return evo_emoji
    else:
        return emoji.NONE

##########################################################################
# df_add_monthevo_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'comptage_1yb' columns and
#   returns a decimal percentage.
#  Returns : float
def df_add_monthevo_column(row):
    if(not str(row['comptage_1yb'])=='nan' and row['comptage_1yb'] > 0):
        evo = (row['comptage'] - row['comptage_1yb'])/row['comptage_1yb']  
        return evo
    else:
        return np.nan
    

##########################################################################
# df_add_monthevotxt_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'comptage_1yb' columns and
#   returns a formated percentage
#  Returns : str
def df_add_monthevotxt_column(row):
    if(not str(row['comptage_1yb'])=='nan' and row['comptage_1yb'] > 0):
        evo = (row['comptage'] - row['comptage_1yb'])/row['comptage_1yb']
        if(evo > 0):
            evo_sign = "+"
        else:
            evo_sign = ""
        return evo_sign+str(round(evo*100,1))+"%"
    else:
        return "N/A"

##########################################################################
# df_add_monthevoemoji_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the evolution between the 'comptage' and 'comptage_1yb' columns and
#   returns the Unicode for the appropriate emoji
#  Returns : str
def df_add_monthevoemoji_column(row):
    if(not str(row['comptage_1yb'])=='nan' and row['comptage_1yb'] > 0):
        evo = (row['comptage'] - row['comptage_1yb'])/row['comptage_1yb']
        if(evo > config.EVO_TOLERANCE):
            evo_emoji = emoji.INCR
        elif(evo < - config.EVO_TOLERANCE):
            evo_emoji = emoji.DECR
        else:
            evo_emoji = emoji.STABLE
        return evo_emoji
    else:
        return emoji.NONE


##########################################################################
# df_add_dayaverage_column(row)
#   Function for the DataFrame apply() method to add a calculated column.
#   Calculates the day average for that row
#  Returns : float
def df_add_dayaverage_column(row):
    if(not str(row['comptage'])=='nan'):
        avg = int(round(row['comptage']/(((row['target_date'] + relativedelta(months=+1)) - row['target_date'] ).days),0))
        return avg
    else:
        return 0

#####################################################################################################################################
# FUNCTIONS FOR RETRIEVING DATA FROM SQL DATABASE                                                                                   #
#####################################################################################################################################

##########################################################################
# sql_get_compteurs_infos(id:str) : 
#  Gets information about a counter from it's ID or nom_axe_abrg
#  Returns : Pandas Dataframe

def sql_get_compteur_infos(id:str):

    query = """SELECT 
                nom_axe_abrg,nom_axe,nom_axe_court,nom_complet,arrondissement,date_installation,latitude_compteur,longitude_compteur
            FROM compteurs where id_compteur='"""+id+"""' OR nom_axe_abrg='"""+id+"""'
            LIMIT 0,1"""

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data    

##########################################################################
# sql_get_city_compteurs() : 
#  Fetches the list of counters in paris
#  Returns : Pandas Dataframe of counters

def sql_get_compteurs(city:str,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = "SELECT nom_axe_abrg FROM `compteurs` WHERE ville='"+city+"'"+inactive_filter+" GROUP BY nom_axe_abrg ORDER BY nom_axe_abrg;"

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data


##########################################################################
# sql_get_city_total_daycount(target_date, city) : 
#  Fetches the sum of counts for a city for a specific date
#  Returns : Integer

def sql_get_city_total_daycount(target_date:datetime,city:str,oneyearoldonly:bool=False,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND cp.actif=True"
    else:
        inactive_filter = ""

    if(oneyearoldonly):
        query  = """
            SELECT ca.ville as ville, SUM(ca.comptage) as comptage 
            FROM vw_comptages_date ca INNER JOIN compteurs cp ON ca.id_compteur=cp.id_compteur
            WHERE ca.ville = '"""+city+"""' AND ca.date = '"""+target_date.strftime("%Y-%m-%d")+"""' AND DATEDIFF('"""+target_date.strftime("%Y-%m-%d")+"""',cp.date_installation) > 365"""+inactive_filter+"""
            GROUP BY ville"""
    else:
        query  = """
            SELECT ca.ville as ville, SUM(ca.comptage) as comptage 
            FROM vw_comptages_date ca INNER JOIN compteurs cp ON ca.id_compteur=cp.id_compteur
            WHERE ca.ville = '"""+city+"""' AND ca.date = '"""+target_date.strftime("%Y-%m-%d")+"""'"""+inactive_filter+"""
            GROUP BY ville"""

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()
    return data['comptage'][0]


##########################################################################
# sql_get_city_total_monthcount(target_date, city) : 
#  Fetches the sum of counts for a city for a specific month
#  Returns : Integer

def sql_get_city_total_monthcount(target_date:datetime,city:str,oneyearoldonly:bool=False,include_inactive:bool=False):
    target_month = str(int(target_date.strftime("%m")))
    target_year  = target_date.strftime("%Y")
    if(not include_inactive):
        inactive_filter = " AND ca.actif=True"
    else:
        inactive_filter = ""

    if(oneyearoldonly):
        query  = """
            SELECT ca.ville as ville, SUM(ca.comptage) as comptage 
            FROM vw_comptages_date ca INNER JOIN compteurs cp ON ca.id_compteur=cp.id_compteur
            WHERE ca.ville = '"""+city+"""' AND MONTH(ca.date) = '"""+target_month+"""' AND YEAR(ca.date) = '"""+target_year+"""' AND DATEDIFF('"""+target_date.strftime("%Y-%m-%d")+"""',cp.date_installation) > 365"""+inactive_filter+"""
            GROUP BY ville"""
    else:
        query  = """
            SELECT ca.ville as ville, SUM(ca.comptage) as comptage 
            FROM vw_comptages_date ca
            WHERE ca.ville = '"""+city+"""' AND MONTH(ca.date) = '"""+target_month+"""' AND YEAR(ca.date) = '"""+target_year+"""'"""+inactive_filter+"""
            GROUP BY ville"""

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()
    return data['comptage'][0]


##########################################################################
# sql_get_city_global_daycount(target_date, city, limit) : 
#  Fetches the sum of counts for each counter for a specific date
#  A limit in number of counters can be provided
#  Returns : Pandas Dataframe of counts by counter

def sql_get_city_global_daycount(target_date:datetime,city:str,limit:int=None,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    if(limit is None):
        limite = ""
    else:
        limite = "\r\nLIMIT 0,"+str(limit)
        
    query  = """
            SELECT nom_axe_abrg, SUM(comptage) as comptage 
            FROM vw_comptages_date
            WHERE ville = '"""+city+"""' AND date = '"""+target_date.strftime("%Y-%m-%d")+"""'"""+inactive_filter+"""
            GROUP BY nom_axe_abrg
            ORDER BY comptage DESC """+limite

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    #Sets the counter name as the index
    data = data.set_index('nom_axe_abrg')
    return data

##########################################################################
# sql_get_city_global_monthcount(target_date, city, limit) : 
#  Fetches the sum of counts for each counter for a specific date
#  A limit in number of counters can be provided
#  Returns : Pandas Dataframe of counts by counter

def sql_get_city_global_monthcount(target_date:datetime,city:str,limit:int=None,include_inactive:bool=False,count_field_name:str="comptage",counters:list=None):
    target_month = str(int(target_date.strftime("%m")))
    target_year  = target_date.strftime("%Y")

    if(limit is None):
        limite = ""
    else:
        limite = "\r\nLIMIT 0,"+str(limit)

    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    if(counters is None):
        compteurs = ""
    else:
        compteurs = " AND nom_axe_abrg IN ('"+"', '".join(counters)+"')"

    query  = """
            SELECT nom_axe_abrg, SUM(comptage) as """+count_field_name+""" 
            FROM vw_comptages_date
            WHERE ville = '"""+city+"""' AND MONTH(date) = '"""+target_month+"""' AND YEAR(date) = '"""+target_year+"""'"""+inactive_filter+compteurs+"""
            GROUP BY nom_axe_abrg
            ORDER BY """+count_field_name+""" DESC """+limite

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    #Sets the counter name as the index
    data = data.set_index('nom_axe_abrg')
    return data


##########################################################################
# sql_get_city_month_ranking(city,count,include_inactive) : 
#  Retreives the ranking of the month count
#  Returns : dataframe

def sql_get_city_month_ranking(city:str,count:int=10,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = """
    SELECT DATE_FORMAT(date,"%Y-%m") as mois, SUM(comptage) as comptage
    FROM vw_comptages_date
    WHERE ville = '"""+city+"""'"""+inactive_filter+""" GROUP BY mois ORDER by comptage DESC, mois DESC LIMIT 0,"""+str(count)
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    #Adds a ranking columon
    data['rank']=np.arange(1,count+1).tolist()

    #Sets the month as the index
    data = data.set_index('mois')
    return data


##########################################################################
# sql_get_city_isrecordville(target_date) : 
#  Checks if the count of a specific date is a record
#  Returns : Bool

def sql_get_city_isrecordville(target_date:datetime,city:str):
    query  = """
        SELECT * FROM
            (SELECT date, SUM(comptage) as total FROM vw_comptages_date where ville = '"""+city+"""' GROUP BY date ORDER by total DESC LIMIT 0,1) record
        WHERE record.date = '"""+target_date.strftime("%Y-%m-%d")+"""'
        """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return len(data) > 0

##########################################################################
# sql_get_isrecordcompteur(target_date,compteur) : 
#  Checks if the count of a specific date is a record for a counter
#  Returns : Bool

def sql_get_isrecordcompteur(target_date:datetime,compteur:str):
    query = """
         SELECT * 
         FROM (SELECT date, SUM(comptage) as total FROM  vw_comptages_date where nom_axe_abrg = '"""+compteur+"""' GROUP BY date ORDER by total DESC LIMIT 0,1) record
         WHERE record.date = '"""+target_date.strftime("%Y-%m-%d")+"""'; 
         """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return len(data) > 0

##########################################################################
# sql_get_city_mmc7jville(target_date) : 
#  Calculates the centered moving average over 7 for the whole town
#  Returns : Float

def sql_get_city_mmc7jville(target_date:datetime,city:str,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""
    query  = """
        SELECT ville,AVG(periode.comptage_compteur) as mmc FROM 
        (
            SELECT ville, date, SUM(comptage) as comptage_compteur 
            FROM vw_comptages_date 
            WHERE  ville = '"""+city+"""'"""+inactive_filter+""" AND DATEDIFF(date,'"""+target_date.strftime("%Y-%m-%d")+"""') >= -3 and DATEDIFF(date,'"""+target_date.strftime("%Y-%m-%d")+"""') <= 3
            GROUP by ville, date
        ) periode
            GROUP BY ville
            ORDER BY ville 
        """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data['mmc'][0]

##########################################################################
# sql_get_city_mmc7jcompteurs(target_date) : 
#  Calculates the centered moving average over 7 for each counter
#  Returns : Dataframe

def sql_get_city_mmc7jcompteurs(target_date:datetime,city:str,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query  = """
        SELECT nom_axe_abrg,AVG(periode.comptage_compteur) as mmc FROM 
        (
            SELECT nom_axe_abrg, date, SUM(comptage) as comptage_compteur 
            FROM vw_comptages_date 
            WHERE  ville = '"""+city+"""'"""+inactive_filter+""" AND DATEDIFF(date,'"""+target_date.strftime("%Y-%m-%d")+"""') >= -3 and DATEDIFF(date,'"""+target_date.strftime("%Y-%m-%d")+"""') <= 3
            GROUP by nom_axe_abrg, date
        ) periode
            GROUP BY nom_axe_abrg
            ORDER BY nom_axe_abrg 
        """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    #Sets the counter name as the index
    data = data.set_index('nom_axe_abrg')
    return data


##########################################################################
# sql_get_city_moyenne3jipcompteurs(target_date) : 
#  Calculates the average of the 3 previous identical days per counter
#  Returns : Dataframe

def sql_get_city_moyenne3jipcompteurs(target_date:datetime,city:str,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""
    query = """
        SELECT nom_axe_abrg, AVG(comptage) as moyenne3jip FROM (
            SELECT date, nom_axe_abrg,SUM(comptage) as comptage
            FROM vw_comptages_date 
            WHERE 
                ville = '"""+city+"""' """+inactive_filter+"""
                AND WEEKDAY(date) = WEEKDAY('"""+datetime.strftime(target_date,"%Y-%m-%d")+"""')
                AND DATEDIFF(date,'"""+datetime.strftime(target_date,"%Y-%m-%d")+"""') >= -21
                AND DATEDIFF(date,'"""+datetime.strftime(target_date,"%Y-%m-%d")+"""') < 0
            GROUP BY date,nom_axe_abrg) a
        GROUP BY nom_axe_abrg; 
    """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    #Sets the counter name as the index
    data = data.set_index('nom_axe_abrg')
    return data

##########################################################################
# sql_get_city_recordvilleprecedent() : 
#  Get information about the previous record
#  Returns : Dataframe
def sql_get_city_recordvilleprecedent(city:str):
    query = "SELECT date, SUM(comptage) as total FROM vw_comptages_date where ville = '"+city+"' GROUP BY date ORDER by total DESC LIMIT 1,1;"

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data

##########################################################################
# sql_get_recordcompteurprecedent(counter:str) : 
#  Get information about the previous record for a specific counter
#  Returns : Dataframe
def sql_get_recordcompteurprecedent(counter:str):
    query = "SELECT nom_axe_court, date, SUM(comptage) as total FROM vw_comptages_date where nom_axe_abrg = '"+counter+"' GROUP BY nom_axe_court,date ORDER by total DESC LIMIT 1,1;"

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data

##########################################################################
# sql_get_city_historique3moisville(target_date:datetime,shift:int=0) : 
#  Gets the history of daily counting over the 3 month before target_date
#  If shift is specified, it will substract this number of days from the
#  begining of the period to allow calculation of centered moving averages
#  Returns : Dataframe
def sql_get_city_historique3moisville(target_date:datetime,city:str,shift:int=0,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = """
        SELECT date, SUM(comptage) as total 
        FROM  vw_comptages_date 
        WHERE date <= '"""+datetime.strftime(target_date,"%Y-%m-%d")+"""'
        AND date >= DATE_SUB(DATE_SUB('"""+datetime.strftime(target_date,"%Y-%m-%d")+"""',INTERVAL 3 MONTH),INTERVAL """+str(shift)+""" DAY)
        AND ville = '"""+city+"""'"""+inactive_filter+"""
        GROUP BY date
        ORDER BY date ASC;"""
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data

##########################################################################
# sql_get_city_history(target_date:datetime,city:string,nbdays:int,shift:int=0,include_inactive:bool=False) : 
#  Gets the history of daily counting for city, from target_date until nbdays after.
#  If shift is specified, it will substract this number of days from the
#  begining of the period and add it to the end to allow calculation of centered moving averages
#  Returns : Dataframe
def sql_get_city_history(target_date:datetime,city:str,nbdays:int,shift:int=0,include_inactive:bool=False):

    start_date = target_date - timedelta(days=shift)
    end_date = target_date + timedelta(days=nbdays+shift)

    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = """
        SELECT date, SUM(comptage) as total 
        FROM  vw_comptages_date 
        WHERE date >= '"""+datetime.strftime(start_date,"%Y-%m-%d")+"""' AND date < '"""+datetime.strftime(end_date,"%Y-%m-%d")+"""' AND ville = '"""+city+"""'"""+inactive_filter+"""
        GROUP BY date
        ORDER BY date ASC;"""
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data

##########################################################################
# sql_get_city_historycompteurs(target_date:datetime,city:string,nbdays:int,shift:int=0,include_inactive:bool=False) : 
#  Gets the history of daily counting for each counter in city, from target_date until nbdays after.
#  If shift is specified, it will substract this number of days from the
#  begining of the period and add it to the end to allow calculation of centered moving averages
#  Returns : Dataframe
def sql_get_city_historycompteurs(target_date:datetime,city:str,nbdays:int,shift:int=0,include_inactive:bool=False,counters:list=None):

    start_date = target_date - timedelta(days=shift)
    end_date = target_date + timedelta(days=nbdays+shift)

    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    if(counters is None):
        limite = ""
    else:
        limite = " AND nom_axe_abrg IN ('"+"', '".join(counters)+"')"

    query = """
        SELECT date, nom_axe_abrg as compteur, SUM(comptage) as total 
        FROM  vw_comptages_date 
        WHERE date >= '"""+datetime.strftime(start_date,"%Y-%m-%d")+"""' AND date < '"""+datetime.strftime(end_date,"%Y-%m-%d")+"""' AND ville = '"""+city+"""'"""+inactive_filter+limite+"""
        GROUP BY date,compteur
        ORDER BY date ASC,compteur ASC;"""
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data



##########################################################################
# sql_get_city_yearhistory(target_date:datetime,not_before:datetime,city:string,shift:int=0,include_inactive:bool=False) : 
#  Gets the history of daily counting for city, from target_date until nbdays after.
#  If shift is specified, it will substract this number of days from the
#  begining of the period and add it to the end to allow calculation of centered moving averages
#  Returns : Dataframe
def sql_get_city_yearhistory(target_date:datetime,not_before:datetime,city:str,shift:int=0,include_inactive:bool=False):

    start_date = not_before - timedelta(days=shift)
    end_date = target_date + relativedelta(months=+1) + timedelta(days=shift)

    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = """
        SELECT YEAR(date) as annee, DAYOFYEAR(date) as jour, SUM(comptage) as total 
        FROM  vw_comptages_date 
        WHERE date >= '"""+datetime.strftime(start_date,"%Y-%m-%d")+"""' AND date < '"""+datetime.strftime(end_date,"%Y-%m-%d")+"""' AND ville = '"""+city+"""'"""+inactive_filter+"""
        GROUP BY annee,jour
        ORDER BY annee,jour ASC;"""
    
    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data

##########################################################################
# sql_get_city_repartitionhoraire(target_date:datetime) : 
#  Gets the count of each hour for each counter on the specified date.
#  The dataframe is pivoted so that each counter is a column and index
#  is the hours
#  Returns : Dataframe
def sql_get_city_repartitionhoraire(target_date:datetime,city:str,counters:list=None,include_inactive:bool=False):
    if(not include_inactive):
        inactive_filter = " AND cp.actif=True"
    else:
        inactive_filter = ""

    if(counters is None):
        limite = ""
    else:
        limite = " AND cp.nom_axe_abrg IN ('"+"', '".join(counters)+"')"
    
    query = """
        SELECT HOUR(ca.date_time) as heure, cp.nom_axe_court as compteur, SUM(ca.comptage) as comptage 
        FROM comptages ca INNER JOIN compteurs cp ON ca.id_compteur=cp.id_compteur 
        WHERE DATE(ca.date_time) = '"""+datetime.strftime(target_date,"%Y-%m-%d")+"""' AND cp.ville = '"""+city+"""'"""+inactive_filter+""" """+limite+"""
        GROUP BY heure,compteur
        ORDER BY heure ASC,compteur ASC; """

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()

    return data.pivot_table('comptage', index='heure', columns='compteur')


##########################################################################
# sql_get_city_bestmonthday(target_date:datetime,city:str,include_inactive:bool) : 
#  Gets the best days and its count for a given month
#  Returns : Dataframe 
def sql_get_city_bestmonthday(target_date:datetime,city:str,include_inactive:bool=False):
    
    target_month = str(int(target_date.strftime("%m")))
    target_year  = target_date.strftime("%Y")

    if(not include_inactive):
        inactive_filter = " AND actif=True"
    else:
        inactive_filter = ""

    query = """
        SELECT date, SUM(comptage) as total 
        FROM  vw_comptages_date 
        WHERE ville = '"""+city+"""' AND MONTH(date) = '"""+target_month+"""' AND YEAR(date) = '"""+target_year+"""'"""+inactive_filter+"""
        GROUP BY date
        ORDER BY total DESC LIMIT 0,1;"""

    sqlEngine    = create_engine(sql_connect_string_ro, pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    data = pd.read_sql(text(query),dbConnection)
    dbConnection.close()
    sqlEngine.dispose()
    return data


