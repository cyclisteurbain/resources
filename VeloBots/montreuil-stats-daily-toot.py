#!/usr/bin/python3

##########################################################################################################################################
# IMPORTS
import argparse

import locale
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import pytz
import urllib.parse

from mastodon import Mastodon

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import seaborn; seaborn.set()

#Importing config file and functions
import xxxx_stats_config as config
from xxxx_stats_functions import *

##########################################################################################################################################
# SETTINGS AND INIT

locale.setlocale(locale.LC_TIME,'fr_FR.utf8')
locale_tz = pytz.timezone("Europe/Paris")

logfile = None
two_ways_record = True 

#Default date
default_date = datetime.strftime(datetime.now() - timedelta(days=1),"%d/%m/%Y")

#Pixel size
px = 1/plt.rcParams['figure.dpi']

#From config file
EVO_TOLERANCE = config.EVO_TOLERANCE
MAX_TOOT_SIZE = config.MAX_TOOT_SIZE

MS_API_URL = config.MS_API_URL
MS_API_TOKEN = config.MS_API_TOKEN

IMG_TEMPDIR = config.IMG_TEMPDIR
GRAPH_HEIGHT = config.GRAPH_HEIGHT
GRAPH_WIDTH = config.GRAPH_WIDTH
GRAPH_TITLE_FONTSIZE = config.GRAPH_TITLE_FONTSIZE
GRAPH_AXES_FONTSIZE = config.GRAPH_AXES_FONTSIZE

CITY_LAT = config.MTL_CITY_LAT
CITY_LONG = config.MTL_CITY_LONG
WEATHER_API_ROOT  = config.WEATHER_API_ROOT
WEATHER_ARCHIVE_API_ROOT = config.WEATHER_ARCHIVE_API_ROOT
DAY_START = config.DAY_START
DAY_END   = config.DAY_END
DAY_RAIN_LIMIT = config.DAY_RAIN_LIMIT
HOUR_RAIN_LIMIT = config.HOUR_RAIN_LIMIT
CEIL_PRECIPITATION = config.CEIL_PRECIPITATION
PRECIP_GRADIENT_LOW = config.PRECIP_GRADIENT_LOW
PRECIP_GRADIENT_HIGH = config.PRECIP_GRADIENT_HIGH

##########################################################################################################################################
# SCRIPT STARTUP
logfile = logmessage("Let's go!")

logmessage("**************************************************************************************",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("* montreuil-daily-stats-toot.py                                                      *",logfile,2)
logmessage("*    Toots daily about bike counter statistics of Montreuil                          *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*   Version 1.0 - 20/11/2022 - Initial version                                       *",logfile,2)
logmessage("*   Version 1.1 - 23/11/2022 - Correction of missing counter bug                     *",logfile,2)
logmessage("*   Version 1.2 - 26/11/2022 - Correction of missing counter bug in month evolution  *",logfile,2)
logmessage("*   Version 1.3 - 18/12/2022 - Adds 3 months evolution and hourly graphs             *",logfile,2)
logmessage("*   Version 1.4 - 22/12/2022 - Code formating                                        *",logfile,2)
logmessage("*   Version 1.5 - 08/03/2023 - Common functions and configuration, weather based info*",logfile,2)
logmessage("*   Version 1.6 - 18/03/2023 - Updated graph sizes                                   *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                                                    *",logfile,2)
logmessage("*                                                 <!            @                    *",logfile,2)
logmessage("*                                                  !___   _____`\<,!_!               *",logfile,2) 
logmessage("*  by @CyclisteUrbain                              !(*)!--(*)---/(*)                 *",logfile,2)
logmessage("**************************************************************************************",logfile,2)
logmessage("Log file : "+logfile,logfile,1)
global_chronostart = datetime.now()
##########################################################################################################################################
# INTERPRETING ARGUMENTS

parser = argparse.ArgumentParser()
parser.add_argument('--date'   ,'-d', help="Date pour laquelle on traite les statistiques. Par défaut, la veille", type= str, default=default_date)
parser.add_argument('--tootit','-t', help="Switch pour indiquer que l'on pouette le résultat",action='store_true', default=False)
args = parser.parse_args()


#Date is an actual date
try:
    target_date = datetime.strptime(args.date,"%d/%m/%Y")
except Exception as oups:
    logmessage("La date fournie '"+args.date+"' est invalide",logfile,5)
    print(parser.format_help())
    quit()  

#Date is in the past
target_date_age = datetime.now() - target_date 
if(target_date_age.total_seconds() < 0):
    logmessage("La date fournie '"+args.date+"' est dans le futur",logfile,5)
    quit()

logmessage("Date : "+datetime.strftime(target_date,"%d-%m-%Y"),logfile,1)

#Do we toot or not?
toot_it = args.tootit
if(toot_it):
    logmessage("Mode TootIt actif : le resultat sera publié dans un pouet",logfile,2)
else:
    logmessage("Mode TootIt inactif : le resultat ne sera pas poueté",logfile,3)

#Initialisation de l'objet mastodon
if(toot_it):
    logmessage("Initialisation de l'objet Mastodon",logfile,1)
    mastodon = Mastodon(access_token = MS_API_TOKEN ,api_base_url=MS_API_URL)

###
# DONNEES METEO
############################################################

#Données de température et de pluviométrie de la journée
logmessage("Récupération des données de température et de pluviométrie de la journée",logfile,1)
meteo_url = WEATHER_API_ROOT+"latitude="+CITY_LAT+"&longitude="+CITY_LONG+"&timezone="+urllib.parse.quote(str(locale_tz.zone), safe='')+"&start_date="+datetime.strftime(target_date,"%Y-%m-%d")+"&end_date="+datetime.strftime(target_date,"%Y-%m-%d")+"&hourly=temperature_2m,apparent_temperature,precipitation&format=csv"
logmessage("  Appel de : "+meteo_url,logfile,1)
try:
    hourly_weather=pd.read_csv(meteo_url,skiprows=2)
    err_meteo = False
    logmessage("  OK : "+str(len(hourly_weather))+" lignes récupérées",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête, les données météo ne seront pas intégrées : "+format(oops),logfile,4)
    err_meteo = True

if(not(err_meteo)):

    for i in range(0,hourly_weather.shape[0]):
        dt = datetime.strptime(hourly_weather.iloc[i,0],"%Y-%m-%dT%H:%M")
        hourly_weather.loc[hourly_weather.index[i],"datetime"] = dt
        hourly_weather.loc[hourly_weather.index[i],"date"] = datetime.date(dt)
        hourly_weather.loc[hourly_weather.index[i],"hour"] = int(datetime.strftime(dt,"%-H"))
        hourly_weather.loc[hourly_weather.index[i],"isDay"] = int(datetime.strftime(dt,"%-H")) >= DAY_START and int(datetime.strftime(dt,"%-H")) <= DAY_END 
        
    day_rain = np.sum(hourly_weather[hourly_weather['isDay']]['precipitation (mm)'])
    temp_min = np.min(hourly_weather[hourly_weather['isDay']]['temperature_2m (°C)'])
    temp_max = np.max(hourly_weather[hourly_weather['isDay']]['temperature_2m (°C)'])
    tmp = hourly_weather[hourly_weather['isDay']]
    nb_heurespluie = len(tmp[tmp['precipitation (mm)']>HOUR_RAIN_LIMIT])
    logmessage("  Pluie : "+str(day_rain)+"mm sur "+str(nb_heurespluie)+"h - Temp. min/max : "+str(temp_min)+"°C/"+str(temp_max)+"°C",logfile,1)

#Code WMO de la journée
logmessage(("Récupération du code WMO du temps de la journée"),logfile,1)
meteo_url = WEATHER_API_ROOT+"latitude="+CITY_LAT+"&longitude="+CITY_LONG+"&timezone="+urllib.parse.quote(str(locale_tz.zone), safe='')+"&start_date="+datetime.strftime(target_date,"%Y-%m-%d")+"&end_date="+datetime.strftime(target_date,"%Y-%m-%d")+"&daily=weathercode&format=csv"
logmessage("  Appel de : "+meteo_url,logfile,1)
try:
    data_day=pd.read_csv(meteo_url,skiprows=2)
    logmessage("  OK : "+str(len(data_day))+" ligne récupérée",logfile,2)

except Exception as oops:
    logmessage("Echec de la requête, les données météo ne seront pas intégrées : "+format(oops),logfile,4)
    err_meteo = True

if(not(err_meteo)):
    wmo_code = int(data_day['weathercode (wmo code)'])
    wmo_emoji = get_weatheremoji(wmo_code)
    logmessage("  Code meteo WMO : "+str(wmo_code)+" "+wmo_emoji,logfile,1)

#Données de pluviométrie des trois mois précédents
logmessage("Récupération des données de pluviométrie des trois mois prédédents",logfile,1)

api_enddate  = datetime.strftime(target_date,"%Y-%m-%d")
api_startdate   = datetime.strftime(target_date + relativedelta(months=-3),"%Y-%m-%d")
meteo_url = WEATHER_API_ROOT+"latitude="+CITY_LAT+"&longitude="+CITY_LONG+"&timezone="+urllib.parse.quote(str(locale_tz.zone), safe='')+"&start_date="+api_startdate+"&end_date="+api_enddate+"&hourly=precipitation&format=csv"

logmessage("  Appel de : "+meteo_url,logfile,1)
try:
    hourly_3mweather=pd.read_csv(meteo_url,skiprows=2)
    logmessage("  OK : "+str(len(hourly_3mweather))+" lignes récupérées",logfile,2)
    err_meteo3m = False

except Exception as oops:
    logmessage("Echec de la requête, les données météo du graph 3 mois ne seront pas intégrées: "+format(oops),logfile,4)
    err_meteo = True

logmessage("  Compilation des données météto des 3 mois précédents",logfile,1)
if(not(err_meteo3m)):
    for i in range(0,hourly_3mweather.shape[0]):
        dt = datetime.strptime(hourly_3mweather.iloc[i,0],"%Y-%m-%dT%H:%M")
        hourly_3mweather.loc[hourly_3mweather.index[i],"datetime"] = dt
        hourly_3mweather.loc[hourly_3mweather.index[i],"date"] = datetime.date(dt)
        hourly_3mweather.loc[hourly_3mweather.index[i],"hour"] = int(datetime.strftime(dt,"%-H"))
        hourly_3mweather.loc[hourly_3mweather.index[i],"isDay"] = int(datetime.strftime(dt,"%-H")) >= DAY_START and int(datetime.strftime(dt,"%-H")) <= DAY_END 
        
    day_precipitations3m = (pd.pivot_table(hourly_3mweather[hourly_3mweather['isDay']], values='precipitation (mm)', index='date', aggfunc='sum')).reset_index()


###
# LISTE DES SITES DE COMPTAGE
############################################################
logmessage("Base de données : liste des sites de comptage (simplifiés en 'compteurs' par la suite)",logfile,1)

try:
    compteurs = sql_get_compteurs("Montreuil")
    if(len(compteurs)==0):
        logmessage("La base de données n'a pas remonté de compteur",logfile,4)
        quit()
    logmessage("  OK : "+str(len(compteurs))+" enregistrements trouvés",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

###
# COMPTAGES DU JOUR
############################################################

# Nombre de passage par compteur
logmessage("Base de données : nombres de passages par compteur",logfile,1)

try:
    daily_counts= sql_get_city_global_daycount(target_date,"Montreuil")
    if(len(daily_counts)==0):
        logmessage("Pas de comptages pour cette date en base de données",logfile,4)
        quit()
    logmessage("  OK : "+str(len(daily_counts))+" enregistrements trouvés",logfile,2)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Calcul du total de la journée
logmessage("Calcul du total de la journée",logfile,1)
total_jour = np.sum(daily_counts['comptage'])
logmessage("   Total: "+str(total_jour),logfile,1)

###
# IDENTIFICATION DES RECORDS
############################################################

#Identifie si c'est un jour de record pour la ville
logmessage("Identifie si c'est un jour de record au niveau ville",logfile,1)

try:
    record_ville = sql_get_city_isrecordville(target_date,"Montreuil")
    if(record_ville):
        logmessage(" Oui! "+emoji.TADA,logfile,1)
except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)

#Identifie les records au niveau compteur
daily_records = []
for counter in daily_counts.index:
    logmessage("Identifie si c'est un record pour le compteur "+counter,logfile,1)
        
    try:
        if(sql_get_isrecordcompteur(target_date,counter)):
            logmessage(" Oui! "+emoji.TADA,logfile,1)
            daily_records.append(counter)
    except Exception as oops:
        logmessage("Echec de la requête: "+format(oops),logfile,5)

###
# MMC7J DU JOUR UN AN AVANT
############################################################
target_date_1yb = target_date - relativedelta(years=1)

logmessage("Calcul de la moyenne mobile centrée sur 7 jours de l'année précédente par compteur",logfile,1)
try:
    daily_mmc=  sql_get_city_mmc7jcompteurs(target_date_1yb,"Montreuil")
    logmessage("  OK : "+str(len(daily_mmc))+" enregistrements calculés",logfile,2)
    
    #Joins the result to the daily count DataFrame
    daily_counts = pd.concat([daily_counts, daily_mmc], axis = 1)

except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

logmessage("Calcul de la moyenne mobile centrée sur 7 jours de l'année précédente globale",logfile,1)
total_daily_mmc = np.sum(daily_counts['mmc'])
logmessage("  OK : "+str(total_daily_mmc)+" passages en moyenne",logfile,1)


###
# CALCUL EVOLUTION 1 AN
############################################################

#Evolution par compteur
logmessage("Calcul de l'évolution sur un an par compteur",logfile,1)

#Evolution en décimale
daily_counts['evo'] = daily_counts.apply(df_add_evo_column, axis = 1)
#Evolution formatée
daily_counts['evotxt'] = daily_counts.apply(df_add_evotxt_column, axis = 1)
#Evolution en emoji
daily_counts['evoemoji'] = daily_counts.apply(df_add_evoemoji_column, axis = 1)


#Evolution globale >>
logmessage("Calcul de l'évolution sur un an globale",logfile,1)
daily_evo_total = (total_jour - total_daily_mmc)/total_daily_mmc
if(daily_evo_total > 0):
    evo_sign = "+"
else:
    evo_sign = ""

#Emoji
if(daily_evo_total > EVO_TOLERANCE):
    daily_evo_total_emoji = emoji.INCR
elif(daily_evo_total < -EVO_TOLERANCE):
    daily_evo_total_emoji = emoji.DECR
else:
    daily_evo_total_emoji = emoji.STABLE

daily_evo_total_str = evo_sign+str(round(daily_evo_total*100,1))+"%"

logmessage("   Evolution globale: "+daily_evo_total_str,logfile,1)

###
# EVOLUTIONS SUR UN MOIS
############################################################

#Evolution par compteur
logmessage("Calcul de l'évolution sur un mois par compteur",logfile,1)

try:
    moyenne_mois_compteurs =  sql_get_city_moyenne3jipcompteurs(target_date,"Montreuil")
    logmessage("OK : "+str(len(moyenne_mois_compteurs))+" enregistrements calculés",logfile,2)

    #Joins the result to the daily count DataFrame
    daily_counts = pd.concat([daily_counts, moyenne_mois_compteurs], axis = 1)    

except Exception as oops:
    logmessage("Echec de la requête: "+format(oops),logfile,5)
    quit()

#Evolution en décimale
daily_counts['evo3jip'] = daily_counts.apply(df_add_evo3jip_column, axis = 1)
#Evolution formatée
daily_counts['evo3jiptxt'] = daily_counts.apply(df_add_evo3jiptxt_column, axis = 1)
#Evolution en emoji
daily_counts['evo3jipemoji'] = daily_counts.apply(df_add_evo3jipemoji_column, axis = 1)

#Evolution globale

logmessage("Calcul de l'évolution sur un mois globale",logfile,1)

moyenne_mois = np.sum(daily_counts['moyenne3jip'])

logmessage(" Moyenne des trois jours identiques précédents : "+str(moyenne_mois),logfile,1)
logmessage(" Calcul de l'évolution",logfile,1)
evo_mois = (total_jour - moyenne_mois)/moyenne_mois

if(evo_mois > 0):
    evo_sign = "+"
else:
    evo_sign = ""

#Emoji
if(evo_mois > EVO_TOLERANCE):
    evo_mois_emoji = emoji.INCR
elif(evo_mois < -EVO_TOLERANCE):
    evo_mois_emoji = emoji.DECR
else:
    evo_mois_emoji = emoji.STABLE

evo_mois_str = evo_sign+str(round(evo_mois*100,1))+"%"

###
# GRAPH DE LA REPARTITON HORAIRE
############################################################
logmessage("Génération du graph de répartition horaire",logfile,1)

#Recuperation des données du graphique
try:
    repartition_horaire = sql_get_city_repartitionhoraire(target_date,"Montreuil")
    logmessage("OK : "+str(len(repartition_horaire))+" enregistrements calculés",logfile,2)
    err_grh = False
except Exception as oops:
    logmessage("Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_grh = True

if(not(err_grh)):
    logmessage("Génération du graphique",logfile,1)
    repartition_horaire = repartition_horaire.reset_index()

    #limite supérieur des ordonnées selon les max dans le dataframe
    ax2top_limit = int(math.ceil((hourly_weather.max(0)['precipitation (mm)']+(hourly_weather.max(0)['precipitation (mm)']/10))))

    totals = []
    for i in range(0,repartition_horaire.shape[0]):
        totals.append(sum(repartition_horaire.iloc[i,1:repartition_horaire.shape[1]]))
    axtop_limit = int(math.ceil((np.max(totals)+(np.max(totals)/10))))


    #Creation du graphique
    if(ax2top_limit>0):
        fig, (ax, ax2) = plt.subplots(2, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px),sharex=True,height_ratios=[4, 1])
    else:
        fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px))

    #Bornes de l'axe des ordonnées
    plt.xlim(right=24)
    plt.xlim(left=-0.5)

    #Formatage des règles et de la guille
    plt.rcParams['xtick.major.size'] = 5
    plt.rcParams['xtick.major.width'] = 2
    plt.rcParams['xtick.bottom'] = True
    plt.rcParams['ytick.left'] = True

    #Formatage 1er graphique
    ax.set_ylim(top=axtop_limit,bottom=0)
    ax.yaxis.set_minor_locator(ticker.MultipleLocator(10))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.grid(visible=True, which='major', color='w', linewidth=1.0)
    ax.grid(visible=True, which='minor', color='w', linewidth=0.5)

    if(ax2top_limit>0):
    #Formatage 2nd graphique
        ax2.set_ylim(top=ax2top_limit,bottom=0)
        ax2.yaxis.set_major_locator(ticker.MultipleLocator(1))
        ax2.yaxis.set_minor_locator(ticker.MultipleLocator(0.25))
        ax2.grid(visible=True, which='major', color='w', linewidth=1.0)
        ax2.grid(visible=True, which='minor', color='w', linewidth=0.5)

    bottom = 0
    i=0
    #Tracés
    for boolean, counter in repartition_horaire.items():
        if(not(boolean)=="heure"):
            ax.bar(repartition_horaire['heure'],counter, label=boolean, bottom=bottom, color=config.GRAPH_COLORS[i])
            bottom += counter
            i+=1

    ax.legend(fontsize=GRAPH_AXES_FONTSIZE)
    if(ax2top_limit>0):
        ax2.bar(hourly_weather['hour'],hourly_weather['precipitation (mm)'],color=[0.341, 0.353, 0.812])

    #Mise en forme des étiquettes
    ax.set_ylabel('Passages',fontdict={'fontsize':GRAPH_AXES_FONTSIZE})
    if(ax2top_limit>0):
        ax2.set_ylabel('Pluie en mm',fontdict={'fontsize':GRAPH_AXES_FONTSIZE})
    ax.set_title('Passages par heure du '+datetime.strftime(target_date,"%A %d %B %Y"),fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    
    if(ax2top_limit>0):
        for label in ax2.get_xticklabels(which='major'):
            label.set(rotation=30, horizontalalignment='center')
 
    #Enregistrement de l'image
    grh_img_filepath = IMG_TEMPDIR+"ms_grh_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("Enregistrement de l'image en tant que "+grh_img_filepath,logfile,1)
    try:
        plt.savefig(grh_img_filepath)
        logmessage(" Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_grh = True

###
# GRAPH DE L'EVOLUTION SUR 3 MOIS
############################################################
logmessage("Génération du graph d'historique sur 3 mois",logfile,1)

#Recuperation des données du graphique
try:
    historique_trois_mois = sql_get_city_historique3moisville(target_date,"Montreuil",3)
    logmessage("OK : "+str(len(historique_trois_mois))+" enregistrements calculés",logfile,2)
    err_ge3m = False
except Exception as oops:
    logmessage("Echec de la requête, le graph ne sera pas généré : "+format(oops),logfile,4)
    err_ge3m = True

if(not(err_ge3m)):
    logmessage("Génération du graphique",logfile,1)
    #Calcul de la MMC7J de chaque entrée
    for i in range(0,historique_trois_mois.shape[0]-6):
        historique_trois_mois.loc[historique_trois_mois.index[i+3],'MMC7'] = np.round(((historique_trois_mois.iloc[i,1]+ historique_trois_mois.iloc[i+1,1] +historique_trois_mois.iloc[i+2,1]+historique_trois_mois.iloc[i+3,1]+historique_trois_mois.iloc[i+4,1]+historique_trois_mois.iloc[i+5,1]+historique_trois_mois.iloc[i+6,1])/7),1)
    #Suppression des 3 premières lignes 
    historique_trois_mois = historique_trois_mois[3:]

    if(not(err_meteo3m)):
        #Tableau du dégradé
        nb_steps = int(CEIL_PRECIPITATION / 0.1)+1
        gradient = get_color_gradient(PRECIP_GRADIENT_LOW,PRECIP_GRADIENT_HIGH,nb_steps)

        #mapping de la pluviometrie en dégradé
        colormap = []
        for dayrain in day_precipitations3m['precipitation (mm)']:
            if(dayrain > CEIL_PRECIPITATION):
                index = int(CEIL_PRECIPITATION*10)
            else:
                index =  int(round(dayrain*10,0))
            colormap.append(gradient[index])

    #Creation du graphique
    fig, ax = plt.subplots(1, 1, figsize=(GRAPH_WIDTH*px, GRAPH_HEIGHT*px), constrained_layout=True)

    plt.rcParams['xtick.major.size'] = 5
    plt.rcParams['xtick.major.width'] = 2
    plt.rcParams['xtick.bottom'] = True
    plt.rcParams['ytick.left'] = True

    if(not(err_meteo3m)):
        ax.bar('date', 'total', data=historique_trois_mois, color=colormap)
    else:
        ax.bar('date', 'total', data=historique_trois_mois, color='grey')

    ax.plot('date', 'MMC7', data=historique_trois_mois, linewidth=3, color=[0, 0.45, 0.45])
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=7))
    ax.yaxis.set_minor_locator(ticker.MultipleLocator(500))

    #grille
    ax.grid(visible=True, which='major', color='w', linewidth=1.0)
    ax.grid(visible=True, which='minor', color='w', linewidth=0.5)

    ax.set_ylabel('Nombre de passages')
    ax.set_xlabel('Dégradé fonction de la pluviometrie en journée',fontdict={'fontsize':GRAPH_AXES_FONTSIZE-2})
    ax.set_title('Evolution des passages sur 3 mois au '+datetime.strftime(target_date,"%A %d %B %Y"), fontdict={'fontsize':GRAPH_TITLE_FONTSIZE})
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    # Rotates and right-aligns the x labels so they don't crowd each other.
    for label in ax.get_xticklabels(which='major'):
        label.set(rotation=30, horizontalalignment='right',fontsize=GRAPH_AXES_FONTSIZE-2)

    #Enregistrement de l'image
    ge3m_img_filepath = IMG_TEMPDIR+"ms_ge3m_"+datetime.strftime(datetime.now(),"%Y%m%d-%H%M%S")+".png"
    logmessage("Enregistrement de l'image en tant que "+ge3m_img_filepath,logfile,1)
    try:
        plt.savefig(ge3m_img_filepath)
        logmessage(" Fichier créé avec succès",logfile,2)
    except Exception as oops:
        logmessage("Impossible de sauvegarder le fichier : "+format(oops),logfile,4)
        err_ge3m = True


###
# POUET PRINCIPAL
############################################################
logmessage("Preparation du premier pouet",logfile,1)

#Est-ce qu'il manque des compteurs -> on compare daily_counts avec une version filtrée des valeurs NaN dans 'comptage'.
if(len(daily_counts)>len(daily_counts.dropna(subset='comptage'))) :
    missing_counters = True
    logmessage("Attention, il manque des données de compteurs",logfile,3)
else:
    missing_counters = False

#Données meteo
if(not(err_meteo)):
    txt_temp = str(temp_min)+"°c/"+str(temp_max)+"°c"
    if(day_rain >= DAY_RAIN_LIMIT):
        txt_pluie = str(round(day_rain,1))+"mm de pluie sur "+str(nb_heurespluie)+"h"
    else:
        txt_pluie = "pas de pluie en journée"
    toot_lignemeteo = "Météo: "+wmo_emoji+" "+txt_temp+", "+txt_pluie+"\r\n"

logmessage("   Informations de comptages et d'evolution sur un an",logfile,1)

target_date_str = datetime.strftime(target_date,"%A %d/%m/%Y")
previous_year   = datetime.strftime(target_date_1yb,"%Y")

toot_text = "#Montreuil"+emoji.PEACH+": Trafic vélo du "+target_date_str+emoji.CYCLST+"\r\n"
toot_text += toot_lignemeteo+"\r\n"
toot_text += str(round(total_jour))+" passages ("+daily_evo_total_emoji+" "+daily_evo_total_str+" par rapport à "+previous_year+")"

#Si certains compteurs manquent, on change les premières lignes
if(missing_counters):
    toot_text = "#Montreuil"+emoji.PEACH+": Trafic vélo du "+target_date_str+emoji.CYCLST+"\r\n"
    toot_text += toot_lignemeteo+"\r\n"
    toot_text += str(round(total_jour))+" passages"

for compteur in compteurs['nom_axe_abrg']:
    logmessage("   Compteur : "+compteur,logfile,1)

    if(not str(daily_counts['comptage'][compteur]) == 'nan'):
        #Comptage
        compteur_comptage = round(daily_counts['comptage'][compteur])
        #Evolution
        compteur_evo = "("+ daily_counts['evotxt'][compteur] +")"
        compteur_emoji = daily_counts['evoemoji'][compteur]
        toot_text += "\r\n"+compteur_emoji+" "+compteur+": "+str(compteur_comptage)+" "+compteur_evo
    else:
        toot_text += "\r\n"+emoji.NOT+" "+compteur+" indisponible"     

#Evolution sur un mois
logmessage("   Informations d'evolution sur un mois",logfile,1)

nom_jour = datetime.strftime(target_date,"%A")

#Si les données de certains compteurs manquent, on remplace la première ligne
if(missing_counters):
    toot_text += "\r\n\r\nÉvolutions sur un mois (ie par rapport aux 3 "+nom_jour+"s précédents):"
else:
    toot_text += "\r\n\r\nÉvolution sur un mois (ie par rapport aux 3 "+nom_jour+"s précédents) : "+evo_mois_emoji+" "+evo_mois_str
     
for compteur in daily_counts.index:
    if(not str(daily_counts['comptage'][compteur]) == 'nan'):
        #Evolution
        compteur_evo = daily_counts['evo3jiptxt'][compteur]
        compteur_emoji = daily_counts['evo3jipemoji'][compteur]
        toot_text += "\r\n"+compteur_emoji+" "+compteur+": "+compteur_evo
    else:
        toot_text += "\r\n"+emoji.NONE+" "+compteur+": N/A" 

#Si c'est un jour de record, on annonce la réponse
if(record_ville or (len(daily_records) > 0) ):
    toot_text += "\r\n"+emoji.DOWN

#Resumé du toot : 
logmessage("Pouet : \n\r"+toot_text,logfile,1)

if(len(toot_text) > MAX_TOOT_SIZE):
    logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
else:
    logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
    #envoi du pouet
    if(toot_it):
        attachment_ids = []
        try:
            #Si on a pu génrer l'image GRH
            if(not err_grh):
                try:
                    logmessage("Envoi de l'image "+grh_img_filepath,logfile,1)
                    attachment = mastodon.media_post(media_file=grh_img_filepath,description="Graphique de la répartition horaire des comptages du "+datetime.strftime(target_date,"%A %d %B %Y")+" pour chaque compteur")
                    attachment_id = str(attachment.id)
                    logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                    attachment_ids.append(attachment_id)
                except Exception as oops:
                    logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)

            #Si on a pu génrer l'image GE3M
            if(not err_ge3m):
                try:
                    logmessage("Envoi de l'image "+ge3m_img_filepath,logfile,1)
                    attachment = mastodon.media_post(media_file=ge3m_img_filepath,description="Graphique de l'évolution du trafic vélo sur les trois mois précédant le "+datetime.strftime(target_date,"%A %d %B %Y")+", en chiffres bruts et en moyenne mobile centrée sur 7 jours")
                    attachment_id = str(attachment.id)
                    logmessage("  Image envoyée : ID "+attachment_id+", URL : "+attachment.url,logfile,2)
                    attachment_ids.append(attachment_id)
                except Exception as oops:
                    logmessage("  Impossible de publier l'image : "+format(oops),logfile,4)


            logmessage("Envoi du pouet",logfile,1)
            toot = mastodon.status_post(toot_text,media_ids=attachment_ids)
            toot_id = toot.id
            logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
        except Exception as oops:
            logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
    else:
        logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
        toot_id = "0000000000000000000"

###
# POUET DE REPONSE SUR LES RECORDS
############################################################
logmessage("Préparation du pouet de réponse s'il y a eu des records battus",logfile,1)
reply_to_toot = toot_id
#Un record pour la ville?
if(record_ville):
    logmessage("Preparation du pouet pour le record 'ville'",logfile,1)

    #Recherche l'ancien record
    logmessage("Récupération des infos du record précédent",logfile,1)
    try:
        previous_record = sql_get_city_recordvilleprecedent("Montreuil")
        previous_record_ville_date      = previous_record['date'][0]
        previous_record_ville_comptages = round(previous_record['total'][0])
        logmessage("Précédent record : "+str(previous_record_ville_comptages)+" le "+datetime.strftime(previous_record_ville_date,"%d/%m/%Y"),logfile,1)
    except Exception as oops:
        logmessage("Echec de la requête: "+format(oops),logfile,4)

    toot_text = emoji.TADA+"C'est un nouveau record pour Montreuil, qui bat le précédent du "+datetime.strftime(previous_record_ville_date,"%d/%m/%Y")+" et ses "+str(previous_record_ville_comptages)+" passages! "+emoji.TADA

    logmessage("Pouet pour le record de ville : \n\r"+toot_text,logfile,1)

#Un record pour un ou plusieurs compteurs? 
if(len(daily_records)>0):
    logmessage("Preparation du pouet pour le ou les records 'compteur'",logfile,1)

    #Construction d'un tableau avec les précédents records
    previous_records_compteur = []
    for compteur in daily_records:
        logmessage("Recherche du precedent record pour "+compteur,logfile,1)
        
        try:
            previous_record = sql_get_recordcompteurprecedent(compteur)
            logmessage("Précédent record pour "+previous_record['nom_axe_court'][0]+" : "+str(round(previous_record['total'][0]))+" le "+datetime.strftime(previous_record['date'][0],"%d/%m/%Y"),logfile,1)
            previous_records_compteur.append(previous_record)
        except Exception as oops:
            logmessage("Echec de la requête: "+format(oops),logfile,4)

    #Préparation du texte du toot

    #Un seul compteur
    if(len(previous_records_compteur)==1):
        nom_compteur = previous_records_compteur[0]['nom_axe_court'][0]
        record = str(round(previous_records_compteur[0]['total'][0]))
        date_record = datetime.strftime(previous_records_compteur[0]['date'][0],"%d/%m/%Y")

        if(record_ville):
            toot_text += "\r\nEt c'est aussi un record pour le compteur "+nom_compteur+" qui bat le précédent du "+date_record+" ("+record+")"+emoji.TADA
        else:
            toot_text = emoji.TADA+"C'est un nouveau record pour le compteur "+nom_compteur+", qui bat le précédent du "+date_record+" et ses "+record+" passages! "+emoji.TADA
    #Plusieurs compteurs
    else:
        if(record_ville):
            toot_text += "\r\nEt c'est aussi un record pour les compteurs"
        else:
            toot_text = emoji.TADA+"C'est un nouveau record pour les compteurs"

        logmessage("Liste des noms de compteurs",logfile,1)
        compteurs_list = ""
        i=1
        for p in previous_records_compteur:
            if(i==len(previous_records_compteur)):
                compteurs_list += " et "
            elif(i==1):
                compteurs_list += " "
            else:
                compteurs_list += ", "
            compteurs_list += p['nom_axe_court'][0]
            i+=1
        
        logmessage("   Liste : "+compteurs_list,logfile,1)
        toot_text += compteurs_list+" qui battent leurs précécents records respectivement des"

        logmessage("Liste des données de compteur",logfile,1)
        compteurs_list = ""
        i=1
        for p in previous_records_compteur:
            if(i==len(previous_records_compteur)):
                compteurs_list += " et "
            elif(i==1):
                compteurs_list += " "
            else:
                compteurs_list += ", "
            compteurs_list += datetime.strftime(p['date'][0],"%d/%m/%Y")+" ("+str(round(p['total'][0]))+")"
            i+=1
        logmessage("   Liste : "+compteurs_list,logfile,1)
        toot_text += compteurs_list+"!"+emoji.TADA

    logmessage("Pouet de records : \n\r"+toot_text,logfile)

    if(len(toot_text) > MAX_TOOT_SIZE):
        logmessage("Longueur du pouet ("+str(len(toot_text))+") depassant les "+str(MAX_TOOT_SIZE)+" caracteres",logfile,4)
    else:
        logmessage("Longueur du pouet ("+str(len(toot_text))+")",logfile,1)
        #envoi du toot
        if(toot_it):
            try:
                logmessage("Envoi du pouet en réponse au toot "+str(reply_to_toot),logfile,1)
                toot = mastodon.status_post(toot_text, in_reply_to_id=reply_to_toot)
                toot_id = toot.id
                logmessage("Pouet envoyé avec succès. ID : "+str(toot_id),logfile,2)
                reply_to_toot = toot_id
            except Exception as oops:
                logmessage("Une erreur s'est produite à l'envoi du pouet : "+format(oops),logfile,4)
        else:
            logmessage("Pouet non envoyé (mode tootIt inactif)",logfile,3)
            toot_id = "0000000000000000001"
            reply_to_toot = toot_id



###
# SCRIPT ENDING
############################################################

global_chronostop = datetime.now()
global_chronotime = global_chronostop - global_chronostart

chronohours, chronoremainder = divmod(global_chronotime.seconds, 3600)
chronominutes, chronoseconds = divmod(chronoremainder, 60)
exectimestr = str(chronohours)+"h"+str(chronominutes)+"m"+str(chronoseconds)+"s"

logmessage("*************************",logfile,1)
logmessage("Script ending after "+exectimestr,logfile,1)