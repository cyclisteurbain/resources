#!/usr/bin/python3

#########################################################################################
# ebcsmfexporter.py                                                                     #
# >> Exports eBike Connect rides and trips to Sigma Data Center SMF files               #
#  @CyclisteUrbain                                                                      #
#                                                                                       #
# v1.0 - 16/07/2022 - Initial version by @CyclisteUrbain                                #
# v1.1 - 11/11/2022 - Supports input with no GPS data                                   #
#                                                                                       #
#                                                             <!            @           #
#                                                              !___   _____`\<,!_!      #
#                                                              !(*)!--(*)---/(*)        #
#########################################################################################

##############################################
# IMPORTS
import argparse
import json
import time
import uuid
from datetime import datetime, timezone
from os import path

import requests

##############################################
# SETTINGS

#Default output location
set_output    = "~/Documents/"
SET_Sports = ["cycling","bmx","racing_bycicle","mountainbike","triathlon","ebike","indoor_cycling","cyclecross","enduro"]
SET_DefaultSport = SET_Sports[0]
SET_DefaultBikeNumber = 3
SET_ReleventGradient = 0.02

#Colors
class fgc:
    HEADER  = '\033[95m'
    OKBLUE  = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL    = '\033[91m'
    ENDC    = '\033[0m'
    BOLD    = '\033[1m'

def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
    return utc + offset

def get_gradient(alt_difference, distance):
    return alt_difference/distance

def distancefromlastpoint(localdata,segment,point,avg):
    if(point < 1): return 0
    else:
        if localdata["speed"][segment][point] is None or localdata["speed"][segment][point-1] is None: return 0
        else:
            speed_ms = (localdata["speed"][segment][point])/3.6
            return (speed_ms*avg/1000)

def is_uphill(localdata,segment,point,avg):
    if(point < 1): return False
    else:
        if localdata["portal_altitudes"][segment][point] is None or localdata["portal_altitudes"][segment][point-1] is None: return False
        else:
            gradient = get_gradient(localdata["portal_altitudes"][segment][point]-localdata["portal_altitudes"][segment][point-1],distancefromlastpoint(localdata,segment,point,avg))
            if gradient>SET_ReleventGradient: return True
            #if localdata["portal_altitudes"][segment][point] > localdata["portal_altitudes"][segment][point-1]+0.7: return True
            else: return False

def is_downhill(localdata,segment,point,avg):
    if(point < 1): return False
    else:
        if localdata["portal_altitudes"][segment][point] is None or localdata["portal_altitudes"][segment][point-1] is None: return False
        else:
            gradient = get_gradient(localdata["portal_altitudes"][segment][point]-localdata["portal_altitudes"][segment][point-1],distancefromlastpoint(localdata,segment,point,avg))
            if gradient<-(SET_ReleventGradient): return True
            else: return False

def assist_level_percentage(localdata,level):
    for i in range(len(localdata["significant_assistance_level_percentages"])):
        if localdata["significant_assistance_level_percentages"][i]['level'] == level:
            return  localdata["significant_assistance_level_percentages"][i]['value']
    return 0

##############################################
# EXECUTION HEADER

print("\r\n"+fgc.HEADER+"#**************************************************************************************#"+fgc.ENDC)
print(fgc.HEADER+"# ebcsmfexporter                                                                       #"+fgc.ENDC)
print(fgc.HEADER+"# Exports eBike Connect rides and trips to Sigma Data Center SMF files                 #"+fgc.ENDC)
print(fgc.HEADER+"#  v1.1 - 11/11/2022                                                                   #"+fgc.ENDC)
print(fgc.HEADER+"#                                                             <!            @          #"+fgc.ENDC)
print(fgc.HEADER+"#                                                              !___   _____`\<,!_!     #"+fgc.ENDC)
print(fgc.HEADER+"# by @CyclisteUrbain                                           !(*)!--(*)---/(*)       #"+fgc.ENDC)
print(fgc.HEADER+"########################################################################################"+fgc.ENDC+"\r\n")

##############################################
# ARGUMENTS CHECK

parser = argparse.ArgumentParser()
parser.add_argument('--id',               '-i', help='ID of the ride or trip to be fetched from eBike Connect', type=int)
parser.add_argument('--cookie',           '-c', help='authentication cookie data', type= str, default= "")
parser.add_argument('--output',           '-o', help='output directory', type= str, default=set_output)
parser.add_argument('--title',            '-t', help='trip title', type= str, default= "")
parser.add_argument('--sport',            '-s', help='trip sport type',type=str,default=SET_DefaultSport,choices=SET_Sports)
parser.add_argument('--bikenumber',       '-b', help='bike number (1-3, 0 for no bike)', type=int, default=SET_DefaultBikeNumber)
parser.add_argument('--descriptionascsv', '-d', help='sets the description as a CSV line of assist usage', action='store_true', default=False)

args = parser.parse_args()


cookiedata = args.cookie
if cookiedata=="":
    print(fgc.FAIL+"Error : cookie data is empty, get it from your browser's devtools"+fgc.ENDC)
    print(parser.format_help())
    quit()

#Output is a directory
outpath = args.output
if not path.isdir(outpath):
    print(fgc.FAIL+"Error : output path "+outpath+" does not exist or is not a directory"+fgc.ENDC)
    print(parser.format_help())
    quit()

#Bike number
bikenumber = args.bikenumber
if bikenumber > 3:
    print(fgc.FAIL+"Error : Bike number must be between 0 and 3, 0 being for no bike"+fgc.ENDC)
    print(parser.format_help())
    quit()

#Ride/Trip ID
ridetrip_id = args.id

#Description as a assist usage CSV
ride_description_as_csv = args.descriptionascsv

##############################################
# FETCHING BIKE RIDE/TRIP DATA

print(fgc.BOLD+"\nCalling eBC API to fetch data for trip with ID "+str(ridetrip_id)+fgc.ENDC)

GET_BaseURL    = "https://www.ebike-connect.com/ebikeconnect/api/activities/trip/details/"
GET_RefBaseURL = "https://www.ebike-connect.com/activities/details/trip/"

URL    = GET_BaseURL+str(ridetrip_id)
RefURL = GET_RefBaseURL+str(ridetrip_id)
headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:102.0) Gecko/20100101 Firefox/102.0',
        'Accept': 'application/vnd.ebike-connect.com.v4+json, application/json',
        'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate, br',
        'Protect-from': 'CSRF',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'If-Modified-Since': 'Mon, 26 Jul 1997 05:00:00 GMT',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Referer': RefURL,
        'Cookie': 'REMEMBER='+cookiedata,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin'
}

response = requests.get(url=URL,headers=headers)
if response.status_code == 200:
    #OK, that's a trip
    ridetrip_data = response.content
    tripride_type = "trip"
    print(fgc.OKGREEN+"    OK"+fgc.ENDC+" : Data for trip "+str(ridetrip_id)+" downloaded")

if response.status_code == 400:
    #Oups, let's try to call it as a ride
    print(fgc.WARNING+"    ID "+str(ridetrip_id)+" doesn't refer to a trip let's try a ride"+fgc.ENDC)
    #Second try
    GET_BaseURL    = "https://www.ebike-connect.com/ebikeconnect/api/activities/ride/details/"
    GET_RefBaseURL = "https://www.ebike-connect.com/activities/details/ride/"

    URL    = GET_BaseURL+str(ridetrip_id)
    RefURL = GET_RefBaseURL+str(ridetrip_id)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:102.0) Gecko/20100101 Firefox/102.0',
        'Accept': 'application/vnd.ebike-connect.com.v4+json, application/json',
        'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate, br',
        'Protect-from': 'CSRF',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache',
        'If-Modified-Since': 'Mon, 26 Jul 1997 05:00:00 GMT',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Referer': RefURL,
        'Cookie': 'REMEMBER='+cookiedata,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin'
    }
    response_ride = requests.get(url=URL,headers=headers)
    if response_ride.status_code == 200:
        #OK, that's a Ride
        ridetrip_data = response_ride.content
        tripride_type = "ride"
        print(fgc.OKGREEN+"    OK"+fgc.ENDC+" : Data for ride "+str(ridetrip_id)+" downloaded")

    if response_ride.status_code == 400:
        #Oups, wrong ID
        print(fgc.FAIL+"    Failure : ID "+str(ridetrip_id)+" doesn't refer to a ride either, please check"+fgc.ENDC)
        quit()

if response.status_code == 403:
    print("   Reason : "+str(response.reason))
    print(fgc.FAIL+"    Failure : Unauthorized, cookie data may not be right or may be outaded"+fgc.ENDC)
    quit()


##############################################
# ANALYSING FETCHED RIDE/TRIP DATA
print(fgc.BOLD+"\nAnalysing downloaded data"+fgc.ENDC)

data = json.loads(ridetrip_data)

#Calculates the average time between each "steps" in altitudes, speeds, cadence... in milliseconds
total_steps = 0
for i in range(len(data["speed"])):          #segments
    total_steps+= len(data["speed"][i])      #count step in segment
data_avg_step = round(int(data['driving_time'])/total_steps)
print ("Average time between data points: "+str(data_avg_step)+"ms")

#TITLE----------------------------------------
if args.title=="":
    if(tripride_type == "ride"):
        ride_title = str(data["title"])
    else:
        ride_title = "Bike trip"
else:
    ride_title = args.title

print (fgc.BOLD+"   Title                 : "+fgc.ENDC+ride_title)

#SPORT----------------------------------------
ride_sport=args.sport
print (fgc.BOLD+"   Sport                 : "+fgc.ENDC+ride_sport)

#BIKE-----------------------------------------
if bikenumber == 0:
    ride_bikenumber = "noBike"
else:
    ride_bikenumber = "bike"+str(bikenumber)
print (fgc.BOLD+"   Bike                  : "+fgc.ENDC+ride_bikenumber)

#START TIME-----------------------------------
#Timestamp in milliseconds > Formated
start_time_ts = data['start_time']
start_time = datetime.fromtimestamp(int(start_time_ts)/1000)
ride_start_date = datetime.strftime(start_time,"%a %b %d %H:%M:%S %Y")
print (fgc.BOLD+"   Start Time            : "+fgc.ENDC+ride_start_date)

#DURATION-------------------------------------
#Milliseconds > centiseconds
ride_duration = str(round(int(data["driving_time"])/10))
print (fgc.BOLD+"   Duration              : "+fgc.ENDC+ride_duration+" centiseconds")

#DISTANCE-------------------------------------
#meter > meter
ride_distance = str(round(data['total_distance']))
print (fgc.BOLD+"   Distance              : "+fgc.ENDC+ride_distance+"m")

#AVERAGE SPEED--------------------------------
#Km/h > m/s
avg_speed = float(data["avg_speed"])
ride_avg_speed = str(round(avg_speed/3.6,1))
print (fgc.BOLD+"   Average speed         : "+fgc.ENDC+ride_avg_speed+"m/s")

#AVERAGE SPEED UPHILL------------------------
#Average of all speeds at a uphill moment
uphillspeed_list = []
for i in range(len(data["speed"])):          #segments
    for j in range(len(data["speed"][i])):   #seconds
        if(is_uphill(data,i,j,data_avg_step)):
            uphillspeed_list.append(data["speed"][i][j])

if(len(uphillspeed_list)>0):
    ride_avg_speed_uphill = str(round(sum(uphillspeed_list) / len(uphillspeed_list)/3.6,1))
    print (fgc.BOLD+"   Average speed uphill  : "+fgc.ENDC+ride_avg_speed_uphill+"m/s")
else:
    ride_avg_speed_uphill = ""
    print (fgc.BOLD+"   Average speed uphill  : "+fgc.ENDC+"unavailable")

#AVERAGE SPEED DOWNHILL------------------------
#Average of all speeds at a down hill moment
dnhillspeed_list = []
for i in range(len(data["speed"])):          #segments
    for j in range(len(data["speed"][i])):   #seconds
        if(is_downhill(data,i,j,data_avg_step)):
            dnhillspeed_list.append(data["speed"][i][j])

if(len(uphillspeed_list)>0):
    ride_avg_speed_downhill = str(round(sum(dnhillspeed_list) / len(dnhillspeed_list)/3.6,1))
    print (fgc.BOLD+"   Average speed down    : "+fgc.ENDC+ride_avg_speed_downhill+"m/s")
else:
    ride_avg_speed_downhill = ""
    print (fgc.BOLD+"   Average speed down    : "+fgc.ENDC+"unavailable")

#MAX SPEED------------------------------------
#Km/h > m/s
max_speed = float(data["max_speed"])
ride_max_speed = str(round(max_speed/3.6,1))
print (fgc.BOLD+"   Max speed             : "+fgc.ENDC+ride_max_speed+"m/s")

#AVERAGE CADENCE------------------------------
#rpm > rpm
ride_avg_cadence = str(round(data['avg_cadence']))
print (fgc.BOLD+"   Average cadence       : "+fgc.ENDC+ride_avg_cadence+"rpm")

#MAX CADENCE-------------------------------
#rpm > rpm
ride_max_cadence = str(round(data['max_cadence']))
print (fgc.BOLD+"   Max cadence           : "+fgc.ENDC+ride_max_cadence+"rpm")

#MAX ALTITUDE------------------------------
#m > m API is bugged, requires to loop through all altitudes
ride_max_altitude = 0
for i in range(len(data["portal_altitudes"])):          #segments
    for j in range(len(data["portal_altitudes"][i])):   #seconds
        if(not(data["portal_altitudes"][i][j] is None)):
            if(data["portal_altitudes"][i][j] > ride_max_altitude):
                ride_max_altitude = data["portal_altitudes"][i][j]

ride_max_altitude = str(round(1000*ride_max_altitude))
print (fgc.BOLD+"   Maximum altitude      : "+fgc.ENDC+ride_max_altitude+"mm")

#ELEVATION GAIN-----------------------------
#m > mm
try:
    ride_elevation_gain = str(round(data['elevation_gain']*1000))
    print (fgc.BOLD+"   Elevation gain        : "+fgc.ENDC+ride_elevation_gain+"mm")
except KeyError:
    ride_elevation_gain = ""
    print (fgc.BOLD+"   Elevation gain        : "+fgc.ENDC+"unavailable")

#ELEVATION LOSS-----------------------------
#m > mm
try:
    ride_elevation_loss = str(round(data['elevation_loss']*1000))
    print (fgc.BOLD+"   Elevation loss        : "+fgc.ENDC+ride_elevation_loss+"mm")
except KeyError:
    ride_elevation_loss = ""
    print (fgc.BOLD+"   Elevation loss        : "+fgc.ENDC+"unavailable")

#AVERAGE CYCLIST POWER----------------------
#Watts > Watts
ride_avg_power = str(round(data['average_driver_power']))
print (fgc.BOLD+"   Average power         : "+fgc.ENDC+ride_avg_power+"W")

#MAXIMUM CYCLIST POWER----------------------
#Watts > Watts
ride_max_power = 0
for i in range(len(data["power_output"])):          #segments
    for j in range(len(data["power_output"][i])):   #seconds
        if(not(data["power_output"][i][j] is None)):
            if(data["power_output"][i][j] > ride_max_power):
                ride_max_power = data["power_output"][i][j]

ride_max_power = str(round(ride_max_power))
print (fgc.BOLD+"   Maximum power         : "+fgc.ENDC+ride_max_power+"W")

#AVERAGE HEART RATE-------------------------------------
ride_avg_heart_rate = str(round(data['avg_heart_rate']))
print (fgc.BOLD+"   Average heart rate    : "+fgc.ENDC+ride_avg_heart_rate+"bpm")

#MAXIMUM HEART RATE-------------------------------------
ride_max_heart_rate = str(round(data['max_heart_rate']))
print (fgc.BOLD+"   Max heart rate        : "+fgc.ENDC+ride_max_heart_rate+"bpm")

#CALORIES-------------------------------------
ride_calories = str(round(data['calories']))
print (fgc.BOLD+"   Calories              : "+fgc.ENDC+ride_calories+"cal")

#CYCLIST/ENGINE POWER RATIO
ride_cyclist_powerratio = str(round(data['total_driver_consumption_percentage'],2))
print (fgc.BOLD+"   Cyclist power ratio   : "+fgc.ENDC+ride_cyclist_powerratio+"%")
ride_engine_powerratio = str(round(data['total_battery_consumption_percentage'],2))
print (fgc.BOLD+"   Engine power ratio    : "+fgc.ENDC+ride_engine_powerratio+"%")

#ASSIST LEVEL PERCENTAGE
ride_assistlevel0 = (round(assist_level_percentage(data,0),2))
ride_assistlevel1 = (round(assist_level_percentage(data,1),2))
ride_assistlevel2 = (round(assist_level_percentage(data,2),2))
ride_assistlevel3 = (round(assist_level_percentage(data,3),2))
ride_assistlevel4 = (round(assist_level_percentage(data,4),2))

if round(ride_assistlevel0) > 0: grph_al0 = "0" + ("-" * (round(ride_assistlevel0)-1))
else: grph_al0 = ""
if round(ride_assistlevel1) > 0: grph_al1 = "1" + ("+" * (round(ride_assistlevel1)-1))
else: grph_al1 = ""
if round(ride_assistlevel2) > 0: grph_al2 = "2" + ("=" * (round(ride_assistlevel2)-1))
else: grph_al2 = ""
if round(ride_assistlevel3) > 0: grph_al3 = "3" + ("*" * (round(ride_assistlevel3)-1))
else: grph_al3 = ""
if round(ride_assistlevel4) > 0: grph_al4 = "4" + ("#" * (round(ride_assistlevel4)-1))
else: grph_al4 = ""

ride_graph_assist_level = "["+grph_al0+grph_al1+grph_al2+grph_al3+grph_al4+"]"

print (fgc.BOLD+"   Assist level graph    : "+fgc.ENDC+ride_graph_assist_level)
print (fgc.BOLD+"   Assist level off      : "+fgc.ENDC+str(ride_assistlevel0)+"%")
print (fgc.BOLD+"   Assist level eco      : "+fgc.ENDC+str(ride_assistlevel1)+"%")
print (fgc.BOLD+"   Assist level tour     : "+fgc.ENDC+str(ride_assistlevel2)+"%")
print (fgc.BOLD+"   Assist level sport    : "+fgc.ENDC+str(ride_assistlevel3)+"%")
print (fgc.BOLD+"   Assist level turbo    : "+fgc.ENDC+str(ride_assistlevel4)+"%")

#ASSISTLEVEL & CYCLIST/ENGINE POWERRATIO CSV
ride_assist_usage_csv = str(ride_cyclist_powerratio)+","+str(ride_engine_powerratio)+","+str(ride_assistlevel0)+","+str(ride_assistlevel1)+","+str(ride_assistlevel2)+","+str(ride_assistlevel3)+","+str(ride_assistlevel4)
print (fgc.BOLD+"   Assist usage CSV      : "+fgc.ENDC+ride_assist_usage_csv)


#EXTERNAL LINK
#links to the eBikeConnect page
ride_external_link = "https://www.ebike-connect.com/activities/details/"+tripride_type+"/"+str(ridetrip_id)
print (fgc.BOLD+"   External link        : "+fgc.ENDC+ride_external_link)

##############################################
# BUILDING XML FILE

fileDate = datetime.strftime(datetime.now(timezone.utc),"%a %b %d %H:%M:%S %Y")
guid = str(uuid.uuid4()).upper()
if ride_description_as_csv:
    ride_description = ride_assist_usage_csv
else:
    ride_description = """
Bosch eBike Connect """+tripride_type+""" ID : """+str(ridetrip_id)+"""
Cyclist/Engine power ratio :
    Cyclist : """+ride_cyclist_powerratio+"""%
    Engine : """+ride_engine_powerratio+"""%
Assist mode usage :
    """+ride_graph_assist_level+"""
    Off : """+str(ride_assistlevel0)+"""%
    Eco : """+str(ride_assistlevel1)+"""%
    Tour : """+str(ride_assistlevel2)+"""%
    Sport : """+str(ride_assistlevel3)+"""%
    Turbo : """+str(ride_assistlevel4)+"""%
"""


XMLData = """<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Activity fileDate=\""""+fileDate+"""\" revision=\"400\">
    <Computer unit="user defined" serial="null" activityType="Cycling" dateCode=""/>
    <GeneralInformation>
        <user color=\"45824\" gender=\"male\">
            <![CDATA[Standard]]>
        </user>
        <sport><![CDATA["""+ride_sport+"""]]></sport>
        <GUID>"""+guid+"""</GUID>
        <bike>"""+ride_bikenumber+"""</bike>
        <name><![CDATA["""+ride_title+"""]]></name>
        <description><![CDATA["""+ride_description+"""]]></description>
        <startDate>"""+ride_start_date+"""</startDate>
        <altitudeDifferencesDownhill>"""+ride_elevation_loss+"""</altitudeDifferencesDownhill>
        <altitudeDifferencesUphill>"""+ride_elevation_gain+"""</altitudeDifferencesUphill>
        <averageCadence>"""+ride_avg_cadence+"""</averageCadence>
        <maximumCadence>"""+ride_max_cadence+"""</maximumCadence>
        <averageHeartrate>"""+ride_avg_heart_rate+"""</averageHeartrate>
        <maximumHeartrate>"""+ride_max_heart_rate+"""</maximumHeartrate>
    <!-- <averageInclineDownhill>0</averageInclineDownhill>    -->
    <!-- <averageInclineUphill>0</averageInclineUphill>        -->
    <!-- <averageRiseRateUphill>0</averageRiseRateUphill>      -->
    <!-- <averageRiseRateDownhill>0</averageRiseRateDownhill>  -->
        <averageSpeed>"""+ride_avg_speed+"""</averageSpeed>
        <maximumSpeed>"""+ride_max_speed+"""</maximumSpeed>
        <averageSpeedDownhill>"""+ride_avg_speed_downhill+"""</averageSpeedDownhill>
        <averageSpeedUphill>"""+ride_avg_speed_uphill+"""</averageSpeedUphill>
        <averagePower>"""+ride_avg_power+"""</averagePower>
        <maximumPower>"""+ride_max_power+"""</maximumPower>
        <maximumAltitude>"""+ride_max_altitude+"""</maximumAltitude>
        <calories>"""+ride_calories+"""</calories>
        <distance>"""+ride_distance+"""</distance>
    <!-- <distanceDownhill>0</distanceDownhill> -->
    <!-- <distanceUphill>0</distanceUphill> -->
        <externalLink><![CDATA["""+ride_external_link+"""]]></externalLink>
        <hrMax>0</hrMax>
        <dataType>memory</dataType>
        <linkedRouteId>0</linkedRouteId>
    <!-- <manualTemperature>0</manualTemperature> -->
    <!--     <maximumInclineDownhill>0</maximumInclineDownhill> -->
    <!--     <maximumInclineUphill>0</maximumInclineUphill> -->
    <!--     <maximumRiseRate>0</maximumRiseRate> -->
    <!--     <maximumTemperature>0</maximumTemperature> -->
    <!--     <minimumRiseRate>0</minimumRiseRate> -->
    <!--     <minimumTemperature>0</minimumTemperature> -->
    <!--     <pauseTime>0</pauseTime> -->
        <rating>1</rating>
        <feeling>2</feeling>
    <!-- <trainingTimeDownhill>0</trainingTimeDownhill> -->
    <!-- <trainingTimeUphill>0</trainingTimeUphill> -->
        <samplingRate>0</samplingRate>
        <statistic>true</statistic>
        <timeOverZone>0</timeOverZone>
        <timeUnderZone>0</timeUnderZone>
        <trackProfile>0</trackProfile>
        <trainingTime>"""+ride_duration+"""</trainingTime>
        <trainingType/>
        <unitId>0</unitId>
        <weather>0</weather>
        <wheelSize>0</wheelSize>
        <wind>0</wind>
        <zone1Start>0</zone1Start>
        <zone2Start>0</zone2Start>
        <zone3Start>0</zone3Start>
        <zone3End>0</zone3End>
        <activityStatus>none</activityStatus>
        <sharingInfo>{\"sigmaStatisticsId\":\"0\",\"komootId\":\"0\",\"stravaId\":\"0\",\"trainingPeaksId\":\"0\",\"twitterId\":\"0\",\"twoPeaksId\":\"0\",\"facebookId\":\"0\"}</sharingInfo>
        <Participant/>
    </GeneralInformation>
</Activity>"""

#Ecriture du XML
outfilename = "Bosch-EBC-"+datetime.strftime(start_time,"%Y%m%d-%H%M%S")+".smf"

with open(outpath+"/"+outfilename, 'w') as outfile:
    outfile.write(XMLData)

print (fgc.OKGREEN+"\r\nData transfered into file "+outpath+"/"+outfilename+fgc.ENDC)
