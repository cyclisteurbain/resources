#!/usr/bin/python3

############################################################################################
# ebc2dc.py                                                                                #
# >> Convert CSV exports from Bosch eBike Connect to SMF files for Sigma Data Center       #
#  @CyclisteUrbain                                                                         #
#                                                                                          #
# v1.0 - 28/03/2022 - Initial version by @CyclisteUrbain                                   #
# v1.1 - 02/04/2022 - EBC titles mapped to Activity name                                   #
# v1.2 - 09/05/2022 - Added Average Power (and waiting for Max Power to be exported)       #
# v1.3 - 12/06/2022 - Added arguments management to override CSV inputs                    #
# v1.4 - 09/07/2022 - Added argument for output directory                                  #
# v1.5 - 14/07/2022 - Added argument sport and bike number, support for DST in start time  #
#                                                                                          #
#                                                                                          #
#                                                             <!            @              #
#                                                              !___   _____`\<,!_!         #
#                                                              !(*)!--(*)---/(*)           #
############################################################################################

##############################################
# IMPORTS
import argparse
import csv
import datetime
import time
import uuid
from collections import namedtuple
from datetime import datetime, timezone
from os import path

##############################################
# SETTINGS

csvheaders = "Start Time,End Time,Driving Time,Type,Status,Total Distance,Calories,Avg. Speed,Avg. Heart Rate,Avg. Cadence,Avg. Altitude,Avg. Driver Power,Max. Speed,Max. Heart Rate,Max. Cadence,Max. Altitude,Ele. Gain,Ele. Loss,BUI Serial,Title"
SET_OutputDir = "~/Documents/EBC2DC"
SET_Sports = [
    "cycling",
    "bmx",
    "racing_bycicle",
    "mountainbike",
    "triathlon",
    "ebike",
    "indoor_cycling",
    "cyclecross",
    "enduro",
]
SET_DefaultSport = SET_Sports[0]
SET_DefaultBikeNumber = 3

# Colors
class fgc:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"


def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
    return utc + offset


##############################################
# EXECUTION HEADER

print(
    "\r\n"
    + fgc.HEADER
    + "#**************************************************************************************#"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# ebc2dc.py                                                                            #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# Converts CSV exports from Bosch ebike connect to SMF XML files for Sigma Data Center #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#  v1.5 - 14/07/2022                                                                   #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#                                                             <!            @          #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#                                                              !___   _____`\<,!_!     #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# by @CyclisteUrbain                                           !(*)!--(*)---/(*)       #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "########################################################################################"
    + fgc.ENDC
    + "\r\n"
)

##############################################
# ARGUMENTS CHECK

parser = argparse.ArgumentParser(
    prog="ebc2dc.py",
    description="Translates Bosch eBike Connect CSV exports to Sigma Data Center SMF files",
)
parser.add_argument(
    "--input", "-i", help="path to Bosch eBike Connect CSV file", type=str, default=""
)
parser.add_argument(
    "--output", "-o", help="path to output directory", type=str, default=SET_OutputDir
)
parser.add_argument("--title", "-t", help="trip title", type=str, default="")
parser.add_argument(
    "--sport",
    "-s",
    help="trip sport type",
    type=str,
    default=SET_DefaultSport,
    choices=SET_Sports,
)
parser.add_argument(
    "--bikenumber",
    "-bn",
    help="bike number (1-3, 0 for no bike)",
    type=int,
    default=SET_DefaultBikeNumber,
)
parser.add_argument(
    "--maximumpower", "-mp", help="maximum power in Watts", type=int, default=0
)

args = parser.parse_args()

if args.input == "":
    print(fgc.FAIL + "Error : no input file was specified" + fgc.ENDC)
    print(parser.format_help())
    quit()

# Check arguments
filepath = args.input
if not path.isfile(filepath):
    print(
        fgc.FAIL
        + "Error : Input file "
        + filepath
        + " does not exist or is not a file"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()

outputdir = args.output
if not path.isdir(outputdir):
    print(
        fgc.FAIL
        + "Error : Ouput directory "
        + outputdir
        + " does not exist or is not a directory"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()
print(fgc.BOLD + "SMF file will be exported to : " + fgc.ENDC + outputdir)

bikenumber = args.bikenumber
if bikenumber > 3:
    print(
        fgc.FAIL
        + "Error : Bike number must be between 0 and 3, 0 being for no bike"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()

##############################################
# CSV FILE LOAD

print(fgc.BOLD + "CSV Input file : " + fgc.ENDC + filepath)

# Cleanup headers

csvheaders = csvheaders.strip()
csvheaders = csvheaders.replace(" ", "_")
csvheaders = csvheaders.replace(".", "")

print(fgc.BOLD + "CSV file headers : " + fgc.ENDC + csvheaders)

##############################################
# CSV FILE ANALYSYS
Headers = namedtuple("Headers", csvheaders, rename=True)
with open(filepath, newline="") as f:
    reader = csv.reader(f)

    # TEST FILE by counting lines
    try:
        i = 0
        f.seek(0)
        next(reader)  # Skip header row.
        for header in map(Headers._make, reader):
            i += 1
        print(
            fgc.OKGREEN
            + "CSV file with "
            + str(i)
            + " line(s) loaded successfully\r\n"
            + fgc.ENDC
        )
    except (TypeError) as err:
        print(
            fgc.FAIL
            + "File ["
            + filepath
            + "] is not a valid eBike Connect CSV file \r\nError : "
            + str(err)
            + fgc.ENDC
        )
        quit()
    except (UnicodeDecodeError) as err:
        print(
            fgc.FAIL
            + "File ["
            + filepath
            + "] appears to be a binary file\r\nError : "
            + str(err)
            + fgc.ENDC
        )
        quit()

    # DATE/TIME * earliest in the file * 2022-03-26 09:02:17 => Sat Mar 26 16:30:12 GMT+0100 2022
    start_time = datetime.now(timezone.utc)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        i_start_time = datetime.strptime(
            header.Start_Time + "+0000", "%Y-%m-%d %H:%M:%S%z"
        )
        if i_start_time < start_time:
            start_time = i_start_time

    start_time = utc2local(start_time)
    #   #Handeling DST
    #   time1 = time.strptime(header.Start_Time+"+0000", '%Y-%m-%d %H:%M:%S%z')
    #   time2 = time.mktime(time1)
    #   if(time.localtime(time2).tm_isdst):
    #       start_time = start_time+timedelta(hours=1)

    print(
        fgc.BOLD
        + "   Activity Start Time : "
        + fgc.ENDC
        + datetime.strftime(start_time, "%a %b %d %H:%M:%S %Y")
    )

    # DURATION * sum of all duration * 0:19:31 => 330000 (1/100th seconds)
    duration = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        time = header.Driving_Time
        duration = duration + sum(
            x * int(t) for x, t in zip([3600, 60, 1], time.split(":"))
        )

    duration = duration * 100
    print(
        fgc.BOLD
        + "   Activity duration : "
        + fgc.ENDC
        + str(duration)
        + " 1/100th seconds"
    )

    # DISTANCE * sum of all distances * 5803 (m) => 5803 (m)
    distance = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        distance = distance + int(header.Total_Distance)

    print(fgc.BOLD + "   Activity distance : " + fgc.ENDC + str(distance) + "m")

    # AVERAGE SPEED * simple distante/time calculation * => m/s
    average_speed = round(100 * distance / duration, 1)

    print(fgc.BOLD + "   Average speed : " + fgc.ENDC + str(average_speed) + "m/s")

    # MAX SPEED * max of all max speeds * km/h => m/s
    max_speed = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        if float(header.Max_Speed) > max_speed:
            max_speed = float(header.Max_Speed)

    max_speed = max_speed / 3.6
    print(fgc.BOLD + "   Activity max speed : " + fgc.ENDC + str(max_speed) + "m/s")

    # UPHILL * sum of all Ele_Gain * m ==> mm
    altitudedifferencesuphill = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        altitudedifferencesuphill = altitudedifferencesuphill + float(header.Ele_Gain)

    altitudedifferencesuphill = int(altitudedifferencesuphill * 1000)
    print(
        fgc.BOLD
        + "   Activity uphill : "
        + fgc.ENDC
        + str(altitudedifferencesuphill)
        + "mm"
    )

    # DOWNHILL * sum of all Ele_Loss* m ==> mm
    altitudedifferencesdownhill = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        altitudedifferencesdownhill = altitudedifferencesdownhill + float(
            header.Ele_Loss
        )

    altitudedifferencesdownhill = int(altitudedifferencesdownhill * 1000)
    print(
        fgc.BOLD
        + "   Activity downhill : "
        + fgc.ENDC
        + str(altitudedifferencesdownhill)
        + "mm"
    )

    # CALORIES * sum of all calories * 0.0 => 0
    calories = 0

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        calories = calories + float(header.Calories)

    calories = round(calories, 0)
    print(fgc.BOLD + "   Calories : " + fgc.ENDC + str(calories) + "kcal")

    # AVERAGE HEARTBEAT RATE * average of HBRs with minutes ponderation * bpm => bpm
    totalhbr = float(0)
    totalminutes = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        time = header.Driving_Time
        minutes = (sum(x * int(t) for x, t in zip([3600, 60, 1], time.split(":")))) / 60
        heartbeats = float(header.Avg_Heart_Rate) * minutes
        totalhbr = totalhbr + heartbeats
        totalminutes = totalminutes + minutes

    average_heartbeat_rate = round(totalhbr / totalminutes, 1)
    print(
        fgc.BOLD
        + "   Average heartbeat rate : "
        + fgc.ENDC
        + str(average_heartbeat_rate)
        + "bpm"
    )

    # MAX HEARTBEAT RATE * max of all HBRs * bpm => bpm
    max_heartbeat_rate = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        if float(header.Max_Heart_Rate) > max_heartbeat_rate:
            max_heartbeat_rate = float(header.Max_Heart_Rate)

    print(
        fgc.BOLD
        + "   Max heartbeat rate : "
        + fgc.ENDC
        + str(max_heartbeat_rate)
        + "bpm"
    )

    # AVERAGE CADENCES * average of cadences with minutes ponderation * rpm => rpm
    totalcadence = float(0)
    totalminutes = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        time = header.Driving_Time
        minutes = (sum(x * int(t) for x, t in zip([3600, 60, 1], time.split(":")))) / 60
        cadence = float(header.Avg_Cadence) * minutes
        totalcadence = totalcadence + cadence
        totalminutes = totalminutes + minutes

    average_cadence = round(totalcadence / totalminutes, 1)
    print(fgc.BOLD + "   Average cadence : " + fgc.ENDC + str(average_cadence) + "rpm")

    # MAX CADENCE * max of all max cadence * rpm => rpm
    max_cadence = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        if float(header.Max_Cadence) > max_cadence:
            max_cadence = float(header.Max_Cadence)

    print(fgc.BOLD + "   Max cadence : " + fgc.ENDC + str(max_cadence) + "rpm")

    # AVERAGE ALTITUDE * average of altitudes with minutes ponderation * m => m
    totalaltitude = float(0)
    totalminutes = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        time = header.Driving_Time
        minutes = (sum(x * int(t) for x, t in zip([3600, 60, 1], time.split(":")))) / 60
        altitude = float(header.Avg_Altitude) * minutes
        totalaltitude = totalaltitude + altitude
        totalminutes = totalminutes + minutes

    average_altitude = round(totalaltitude / totalminutes, 1)
    print(fgc.BOLD + "   Average altitude : " + fgc.ENDC + str(average_altitude) + "m")

    # MAX ALTITUDE * max of all max altitude * m => m
    max_altitude = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        if float(header.Max_Altitude) > max_altitude:
            max_altitude = float(header.Max_Altitude)

    print(fgc.BOLD + "   Max altitude : " + fgc.ENDC + str(max_altitude) + "m")

    # AVERAGE POWER * average of power with minutes ponderation * w => w
    totalpower = float(0)
    totalminutes = float(0)

    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        time = header.Driving_Time
        minutes = (sum(x * int(t) for x, t in zip([3600, 60, 1], time.split(":")))) / 60
        power = float(header.Avg_Driver_Power) * minutes
        totalpower = totalpower + power
        totalminutes = totalminutes + minutes

    average_power = round(totalpower / totalminutes, 1)
    print(fgc.BOLD + "   Average power : " + fgc.ENDC + str(average_power) + "W")

    # MAXIMUM POWER
    # Was it added to arguments
    if args.maximumpower != 0:
        max_Power = args.maximumpower
    else:
        max_Power = 0

    print(fgc.BOLD + "   Maximum Power : " + fgc.ENDC + str(max_Power) + "W")

    # BUI SERIAL NUMBER * last non empty BUI serial number
    serial_number = ""
    f.seek(0)
    next(reader)  # Skip header row.
    for header in map(Headers._make, reader):
        if header.BUI_Serial != "":
            serial_number = header.BUI_Serial

    print(fgc.BOLD + "   Device serial number: " + fgc.ENDC + serial_number)

    # ANALYSE : Titres

    # Overloaded by arguments?
    if args.title != "":
        title = args.title
    else:
        title = ""
        new_title = ""
        f.seek(0)
        next(reader)  # Skip header row.
        for header in map(Headers._make, reader):
            prev_title = new_title
            new_title = header.Title
            if prev_title != new_title:
                title = title + "-" + new_title
        title = title[1:]

    print(fgc.BOLD + "   Title: " + fgc.ENDC + title)


# << with open(filepath, newline='') as f:

# SPORT (checks and default are aleady handeled by argparse)
sport = args.sport
print(fgc.BOLD + "   Sport: " + fgc.ENDC + sport)

# BIKE NUMBER
if bikenumber == 0:
    bikenumber_str = "noBike"
else:
    bikenumber_str = "bike" + str(bikenumber)
print(fgc.BOLD + "   Bike: " + fgc.ENDC + bikenumber_str)

# Static pre-set values
fileDate = datetime.strftime(datetime.now(timezone.utc), "%a %b %d %H:%M:%S %Y")
unit = "Bosch Kiox"
guid = str(uuid.uuid4()).upper()
description = (
    "Trip : "
    + title
    + "\r\nAverage Power : "
    + str(average_power)
    + "W"
    + "\r\nUnit : "
    + unit
    + " "
    + serial_number
)

# XML File build

XMLData = (
    """<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Activity fileDate=\""""
    + fileDate
    + """\" revision=\"400\">
    <Computer unit="user defined" serial="null" activityType="Cycling" dateCode=""/>
    <GeneralInformation>
        <user color=\"45824\" gender=\"male\">
            <![CDATA[Standard]]>
        </user>
        <sport><![CDATA["""
    + sport
    + """]]></sport>
        <GUID>"""
    + guid
    + """</GUID>
        <altitudeDifferencesDownhill>"""
    + str(altitudedifferencesdownhill)
    + """</altitudeDifferencesDownhill>
        <altitudeDifferencesUphill>"""
    + str(altitudedifferencesuphill)
    + """</altitudeDifferencesUphill>
        <averageCadence>"""
    + str(average_cadence)
    + """</averageCadence>
        <averageHeartrate>"""
    + str(average_heartbeat_rate)
    + """</averageHeartrate>
        <averageInclineDownhill>0</averageInclineDownhill>
        <averageInclineUphill>0</averageInclineUphill>
        <averageRiseRateUphill>0</averageRiseRateUphill>
        <averageRiseRateDownhill>0</averageRiseRateDownhill>
        <averageSpeed>"""
    + str(average_speed)
    + """</averageSpeed>
        <averageSpeedDownhill>0</averageSpeedDownhill>
        <averageSpeedUphill>0</averageSpeedUphill>
        <averagePower>"""
    + str(average_power)
    + """</averagePower>
        <bike>"""
    + bikenumber_str
    + """</bike>
        <calories>"""
    + str(calories)
    + """</calories>
        <dataType>memory</dataType>
        <description><![CDATA["""
    + description
    + """]]></description>
        <distance>"""
    + str(distance)
    + """</distance>
        <distanceDownhill>0</distanceDownhill>
        <distanceUphill>0</distanceUphill>
        <externalLink><![CDATA[]]></externalLink>
        <hrMax>0</hrMax>
        <linkedRouteId>0</linkedRouteId>
        <manualTemperature>0</manualTemperature>
        <maximumAltitude>0</maximumAltitude>
        <maximumCadence>"""
    + str(max_cadence)
    + """</maximumCadence>
        <maximumHeartrate>"""
    + str(max_heartbeat_rate)
    + """</maximumHeartrate>
        <maximumInclineDownhill>0</maximumInclineDownhill>
        <maximumInclineUphill>0</maximumInclineUphill>
        <maximumRiseRate>0</maximumRiseRate>
        <maximumSpeed>"""
    + str(max_speed)
    + """</maximumSpeed>
        <maximumTemperature>0</maximumTemperature>
        <minimumRiseRate>0</minimumRiseRate>
        <minimumTemperature>0</minimumTemperature>
        <maximumPower>"""
    + str(max_Power)
    + """</maximumPower>
        <name><![CDATA["""
    + title
    + """]]></name>
        <pauseTime>0</pauseTime>
        <rating>1</rating>
        <feeling>2</feeling>
        <trainingTimeDownhill>0</trainingTimeDownhill>
        <trainingTimeUphill>0</trainingTimeUphill>
        <samplingRate>0</samplingRate>
        <startDate>"""
    + datetime.strftime(start_time, "%a %b %d %H:%M:%S %Y")
    + """</startDate>
        <statistic>true</statistic>
        <timeOverZone>0</timeOverZone>
        <timeUnderZone>0</timeUnderZone>
        <trackProfile>0</trackProfile>
        <trainingTime>"""
    + str(duration)
    + """</trainingTime>
        <trainingType/>
        <unitId>0</unitId>
        <weather>0</weather>
        <wheelSize>0</wheelSize>
        <wind>0</wind>
        <zone1Start>0</zone1Start>
        <zone2Start>0</zone2Start>
        <zone3Start>0</zone3Start>
        <zone3End>0</zone3End>
        <activityStatus>none</activityStatus>
        <sharingInfo>{\"sigmaStatisticsId\":\"0\",\"komootId\":\"0\",\"stravaId\":\"0\",\"trainingPeaksId\":\"0\",\"twitterId\":\"0\",\"twoPeaksId\":\"0\",\"facebookId\":\"0\"}</sharingInfo>
        <Participant/>
    </GeneralInformation>
</Activity>"""
)


# Ecriture du XML
outfilename = "BoschEBC-" + datetime.strftime(start_time, "%Y%m%d-%H%M%S") + ".smf"

with open(outputdir + "/" + outfilename, "w") as outfile:
    outfile.write(XMLData)

print(
    fgc.OKGREEN
    + "\r\nData transfered into file "
    + outputdir
    + "/"
    + outfilename
    + fgc.ENDC
)
